//
//  HomeCollectionViewCell.swift
//  Digities
//
//  Created by vamsi on 04/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    func configureCell(_ data:HomeModel){
        imageView.image = data.image
        titleLabel.text = data.title.rawValue
    }
}
