//
//  NetworkManager.swift
//  GreenApple
//
//  Created by vamsi on 03/05/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation
import Alamofire

let headers: HTTPHeaders = [
    "Content-Type":"application/json"
]

typealias successs = (Data)-> Void
typealias failure = (DataNotFound)-> Void


class NetworkManager{
    
    static  func afgetRequest(url:String,paramaters:[String:Any],success:@escaping(Data)-> () , failure: @escaping(Error)->()){
        print("url---*****--------******------------",url)
        print("parameters---*****--------******------------",paramaters)
        print(" ------ GET Method ------")
        Alamofire.request(url, method: .get, parameters: paramaters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in            switch response.result{
            case .success(let value):
                print(value)
                if let response = value as? [String:Any]{
                    guard let respData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) else{return}
                    success(respData)
            }
            case .failure(let error):
                print(error)
                failure(error)
            }
        }
    }
    
    
    static func afpostRequest(url:String,paramaters:[String:Any],success:@escaping(Data)-> () , failure: @escaping(Error)->()){
        print("url---*****--------******------------",url)
        print("parameters---*****--------******------------",paramaters)
        print(" ------ POST Method ------")
        Alamofire.request(url, method: .post, parameters: paramaters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response)
            switch response.result{
            case .success(let value):
                print(value)
                if let response = value as? [String:Any]{
                    guard let respData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted) else{return}
                    success(respData)

                }
            case .failure(let error):
                print(error.localizedDescription)
                failure(error)
            }
        }
        
        
    }
    
    static func getReponse(url:String,
                           success:@escaping successs,
                           failure:@escaping failure){
        print("url---*****--------******------------",url)
        print(" ------ GET Method ------")
        guard let url = URL(string: url) else {
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        URLSession.shared.dataTask(with: urlRequest) { (data, resp, err) in
            guard let data = data else{
                failure(.dataNotFound)
                return
            }
          success(data)
        }.resume()
    }
    
    static func postResponse(url:String,
                        parameters:[String:Any],
                        success:@escaping successs,
                        failure:@escaping failure){
        print("url---*****--------******------------",url)
        print("parameters---*****--------******------------",parameters)
        print(" ------ POST Method ------")
        guard let url = URL(string: url) else {return}
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        guard let paramsData = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) else{return}
        urlRequest.httpBody = paramsData
        URLSession.shared.dataTask(with: urlRequest) { (data, resp, err) in
            guard let jsonData = data else{
                failure(.dataNotFound)
                return
            }
            success(jsonData)
            }.resume()
    }
    
    
    
    
    
    
    
    
}

func GetMethodByusingUrlSession(url:String,completionHandler: @escaping ([String:Any]) -> ()) {
    
    showLoader()
    
    guard let url = URL(string:url) else { return }
    
    let session = URLSession.shared
    session.dataTask(with: url) { (data, response, error) in
        if let response = response {
            //                print(response)
        }
        
        if let data = data {
            
            DispatchQueue.main.async {
                hideLoader()
            }
            //                print(data)
            do {
                
                DispatchQueue.main.async {
                    hideLoader()
                }
                
//                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                let json = try  JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]

                print(json)
                completionHandler(json)

            } catch {
                
                DispatchQueue.main.async {
                    hideLoader()
                }
                print(error)
            }
            
        }
        }.resume()

}

func POSTMethodByusingUrlSession(url:String,Prameters:[String:Any],completionHandler: @escaping ([String:Any]) -> ()) {
    showLoader()
    
    
    let parameters = Prameters
    
    guard let url = URL(string: url) else { return }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

    guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
    request.httpBody = httpBody
    
    let session = URLSession.shared
    session.dataTask(with: request) { (data, response, error) in
        DispatchQueue.main.async {
            hideLoader()
        }
        if let response = response {
            print(response)
        }
        
        if let data = data {
            do {
                
                DispatchQueue.main.async {
                    hideLoader()
                }
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                print(json)
                
                completionHandler(json)
            } catch {
                print(error)
                
                DispatchQueue.main.async {
                    hideLoader()
                }
            }
        }
        
        }.resume()
    
    


}
