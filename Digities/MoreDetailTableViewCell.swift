//
//  MoreDetailTableViewCell.swift
//  Digities
//
//  Created by vamsi on 11/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit
import SDWebImage

class MoreDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topImgView: UIImageView!
    @IBOutlet weak var draftLbl: UILabel!
    @IBOutlet weak var expenseDateLbl: UILabel!
    @IBOutlet weak var expenseCategoryLbl: UILabel!
    @IBOutlet weak var expenseSubCategoryLbl: UILabel!
    
    @IBOutlet weak var noOfNightsLbl: UILabel!
    @IBOutlet weak var expenseAmountLbl: UILabel!
    @IBOutlet weak var expenseRemarksLbl: UILabel!
    @IBOutlet weak var reimbursableLbl: UILabel!
    @IBOutlet weak var reimAmountLbl: UILabel!
    @IBOutlet weak var chargeableAmntLbl: UILabel!
    @IBOutlet weak var bottomImgView: UIImageView!
    @IBOutlet weak var bottomImaView2: UIImageView!
    
    var isFrom:TitleType?
    var vc:MoreDetailClaimViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(expDetails:ExpenseData,title:TitleType?){
        isFrom = title
        setupCellStyle(with: title)
//        topImgView.image = #imageLiteral(resourceName: "Left_menu_viewall_claims")
        expenseDateLbl.text = (expDetails.expense_date ?? "").getStringWithDateFormatter()
        expenseCategoryLbl.text = expDetails.exp_cat_name ?? ""
        noOfNightsLbl.text = expDetails.no_of_nights ?? ""
        expenseSubCategoryLbl.text = expDetails.exp_sub_cat_name ?? ""
        if let currency = defaults.value(forKey: "currency") as? String{
            expenseAmountLbl.text = currency + (expDetails.exp_amount ?? "")
            reimAmountLbl.text = currency + (expDetails.reimbursable_amount ?? "")
        }
        
        expenseRemarksLbl.text = expDetails.expense_remarks ?? ""
        
        if expDetails.reimbursable == "1"{
            reimbursableLbl.text = "Yes"

        }else{
            reimbursableLbl.text = expDetails.reimbursable

        }
        
        if expDetails.chargable_to_client == "1"{
            chargeableAmntLbl.text = "Yes"

        }else{
            chargeableAmntLbl.text = expDetails.chargable_to_client

        }
        

        
        expenseRemarksLbl.text = expDetails.expense_remarks ?? ""
        expenseRemarksLbl.text = expDetails.expense_remarks ?? ""
        let tap = UITapGestureRecognizer(target: self, action: #selector(zoomBottomImage))
        bottomImgView.addGestureRecognizer(tap)
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(zoomTopImage))
        topImgView.addGestureRecognizer(tapGest)
    }
    
    
    @objc func zoomBottomImage(){
        if let vc = vc{
            bottomImgView.zoomAnimation(inViewController: vc)
        }
    }
    @objc func zoomTopImage(){
        if let vc = vc{
            topImgView.zoomAnimation(inViewController: vc)
        }
    }
    
    
    func setupClaimBill(claimDetails:ClaimBillData){
        //        let url =  "http://projectdemo.co.in/expensetracking/Androidapi/uploads/Fast_food_meal.jpg"
        //       let url = " http://projectdemo.co.in/expensetracking/assets/uploads/expense/bills/Fast_food_meal.jpg"
        if  let imageName = claimDetails.original{
            if let url = URL(string: BaseUrl.getImagePath + imageName){
                bottomImgView.sd_setImage(with: url, completed: nil)
            }
        }
    }
    
    func setupCellStyle(with title:TitleType?){
        setupUI()
        guard let title = title else{return}
        switch title {
        case .draft:
            containerView.backgroundColor = UIColor.gray.withAlphaComponent(0.2)
        case .pending:
            containerView.backgroundColor = UIColor.yellow.withAlphaComponent(0.2)
        case .rejected:
            containerView.backgroundColor = UIColor.red.withAlphaComponent(0.2)
        case .approved:
            containerView.backgroundColor = UIColor.green.withAlphaComponent(0.2)
        case .processed:
            containerView.backgroundColor = UIColor.blue.withAlphaComponent(0.2)
        case .viewAllClaims:
            containerView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        default:
            return
        }
        
        
    }
    
    
    func setupUI(){
        containerView.layer.cornerRadius = 25
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowRadius = 0.1
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
    }
    
    
    
    
}





