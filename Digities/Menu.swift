//
//  Menu.swift
//  GreenApple
//
//  Created by vamsi on 02/05/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation
import UIKit

enum TitleType:String{
    case create = "Create Claim"
    case draft = "Draft Claims"
    case rejected   = "Rejected Claims"
    case processed = "Processed Claims"
    case address = "Address Book"
    case pending = "Pending Claims"
    case approved = "Approved Claims"
    case viewAllClaims = "View all Claims"
    case signout = "Sign out"
}

struct HomeModel {
    var image:UIImage
    var title:TitleType
}

struct GetHomeModel{
    
    static func getHomeDetails() -> [HomeModel]{
        let create = HomeModel(image: #imageLiteral(resourceName: "createclaim"), title: .create)
        let address = HomeModel(image: #imageLiteral(resourceName: "addressbook"), title: .address)
        let draft = HomeModel(image: #imageLiteral(resourceName: "draftclaimsold"), title: .draft)
        let pending = HomeModel(image: #imageLiteral(resourceName: "pendingclaimsandbox"), title: .pending)
        let rejected = HomeModel(image: #imageLiteral(resourceName: "rejectclaim"), title: .rejected)
        let approved = HomeModel(image: #imageLiteral(resourceName: "pendingclaim"), title: .approved)
        let processed = HomeModel(image: #imageLiteral(resourceName: "processedclaims"), title: .processed)
        let viewAllClaims = HomeModel(image: #imageLiteral(resourceName: "viewallclaims"), title: .viewAllClaims)
        return [create,address,draft,pending,rejected,approved,processed,viewAllClaims]
    }
    
    static func getSideMenuDetails() -> [HomeModel]{
        let create = HomeModel(image: #imageLiteral(resourceName: "createclaim"), title: .create)
        let address = HomeModel(image: #imageLiteral(resourceName: "addressbook"), title: .address)
        let draft = HomeModel(image: #imageLiteral(resourceName: "draftclaimsold"), title: .draft)
        let pending = HomeModel(image: #imageLiteral(resourceName: "pendingclaimsandbox"), title: .pending)
        let rejected = HomeModel(image: #imageLiteral(resourceName: "rejectclaim"), title: .rejected)
        let approved = HomeModel(image: #imageLiteral(resourceName: "pendingclaim"), title: .approved)
        let processed = HomeModel(image: #imageLiteral(resourceName: "processedclaims"), title: .processed)
        let viewAllClaims = HomeModel(image: #imageLiteral(resourceName: "viewallclaims"), title: .viewAllClaims)
        let signout = HomeModel(image: #imageLiteral(resourceName: "signout"), title: .signout)
        return [create,address,draft,pending,rejected,approved,processed,viewAllClaims,signout]
    }
}

