//
//  AllMessagesTableViewCell.swift
//  Digities
//
//  Created by vamsi on 14/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class AllMessagesTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var subjectLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(_ data:AllNotificationsData){
        setupUI()
        subjectLbl.text = data.subject ?? ""
        messageLbl.text = data.message ?? ""
        if let status = data.status{
            statusLbl.text = status == "1" ? "Read" : "Unread"
        }
    }
    func setupUI(){
        containerView.layer.cornerRadius = 22
//        containerView.layer.shadowColor = UIColor.red.cgColor
        containerView.layer.shadowRadius = 0.1
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
    }
   
}
