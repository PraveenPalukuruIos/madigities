//
//  String.swift
//  Digities
//
//  Created by vamsi on 08/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation

extension String{
    func getStringWithDateFormatter(formate:String = "dd/MM/yyyy") -> String{
            var finalString = ""
            let formatter = DateFormatter()
            let gregorianCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            formatter.calendar = gregorianCalendar as Calendar
            formatter.timeZone = NSTimeZone.local
            formatter.locale = Locale.current
            formatter.dateFormat = "yyyy-MM-dd"
        if let convertedDate = formatter.date(from: self){
            formatter.dateFormat = formate
             let str = formatter.string(from: convertedDate)
            finalString = str
        }
            return finalString
        }
}

public func SearchBarCustom(sc:UISearchBar){
if #available(iOS 11.0, *) {
    let sc = UISearchController(searchResultsController: nil)
//    sc.delegate = self
    let scb = sc.searchBar
    scb.tintColor = UIColor.white
    scb.barTintColor = UIColor.white
    
    if let textfield = scb.value(forKey: "searchField") as? UITextField {
        textfield.textColor = UIColor.blue
        if let backgroundview = textfield.subviews.first {
            
            // Background color
            backgroundview.backgroundColor = UIColor.white
            
            // Rounded corner
            backgroundview.layer.cornerRadius = 10;
            backgroundview.clipsToBounds = true;
        }
    }
    
    

}
}

// public func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
//    let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert);
//    vc.present(alert, animated: true, completion: nil)
//}

extension UIViewController {
    
    func showAlertMessage(message: String, title: String ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

