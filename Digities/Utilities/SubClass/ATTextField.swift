//
//  ATTextField.swift
//  GreenApple
//
//  Created by vamsi on 11/05/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

@IBDesignable
class ATTextField: UITextField {

    @IBInspectable var placeholderTextColor:UIColor = .black{
        didSet{
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSAttributedString.Key.foregroundColor : placeholderTextColor])
        }
    }
  
    @IBInspectable var borderWidth:CGFloat = 0.0{
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    
    @IBInspectable var borderColor:UIColor = .black{
        didSet{
            layer.borderColor = borderColor.cgColor
        }
    }

    @IBInspectable var cornerRadius: CGFloat = 0 {
        
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    
}
