//
//  ATSearchBar.swift
//  Digities
//
//  Created by vamsi on 11/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

@IBDesignable class ATSearchBar: UISearchBar {
    
    @IBInspectable var cornerRadius:CGFloat = 10.0{
        didSet{
            layer.cornerRadius = cornerRadius
        }
    }
   
//    @IBInspectable var borderWidth:CGFloat = 1.0{
//        didSet{
//            layer.borderWidth = borderWidth
//        }
//    }
//
//    @IBInspectable var borderColor:UIColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1){
//        didSet{
//            layer.borderColor = borderColor.cgColor
//        }
//    }
    
    @IBInspectable var shadowRadius:CGFloat = 0.0{
        didSet{
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity:Float = 0.0{
        didSet{
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var shadowOffset:CGSize = CGSize(width: 0, height: 0){
        didSet{
            layer.shadowOffset = shadowOffset
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}

