//
//  DateFormatter+Extension.swift
//  HungrayHub
//
//  Created by Tejinder Paul Singh on 30/11/18.
//  Copyright © 2018 Aisha Technologies. All rights reserved.
//

import Foundation
import UIKit

extension Calendar {
    
    static func defaultCalendar() -> Calendar{
        return Calendar(identifier: .gregorian)
    }
    
}

extension DateFormatter {
    
    private static let genericFormatter = DateFormatter.defaultDateFormatter()
    
    class func defaultDateFormatter() -> DateFormatter {
        
        let formatter = DateFormatter()
        
        let gregorianCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        
        formatter.calendar = gregorianCalendar as Calendar
       // formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        //formatter.locale = Locale(identifier: "en_US_POSIX")

        formatter.timeZone = NSTimeZone.local
        formatter.locale = Locale.current
        
        formatter.dateFormat = "dd/MM/yyyy"
        
        return formatter
    }
    
    //MARK: public interface
    
    class func ISOStringFromDate(date:NSDate) -> String {
        
        return self.genericFormatter.string(from: date as Date)
    }
    
    class func dateFromISOString(date:String) -> NSDate {
        
        return self.genericFormatter.date(from: date)! as NSDate
    }
    
class func filterDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        let gregorianCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        formatter.calendar = gregorianCalendar as Calendar
    
//        formatter.locale = Locale(identifier: "en_US_POSIX")
//        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
    formatter.timeZone = NSTimeZone.local
    formatter.locale = Locale.current
    
        formatter.dateFormat = "dd MMM yyyy"
        
        return formatter
    }
    
    
    class func displayFilterDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        let gregorianCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        formatter.calendar = gregorianCalendar as Calendar
        
//        formatter.locale = Locale(identifier: "en_US_POSIX")
//        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
        formatter.timeZone = NSTimeZone.local
        formatter.locale = Locale.current
        
        
        formatter.dateFormat = "dd MMM"

        return formatter
    }
}

