//
//  RoundImage.swift
//  Digities
//
//  Created by vamsi on 03/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit


@IBDesignable class RoundImage: UIImageView {
    
    @IBInspectable var cornerRadius:CGFloat = 10{
        didSet{
            setupRoundImage()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRoundImage()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupRoundImage()
    }
    
    func setupRoundImage(){
        layer.masksToBounds = true
        layer.cornerRadius = frame.size.height/2
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 10
        layer.shadowOpacity = 1.0
        layer.shadowOffset = CGSize(width: 5, height: 5)
        
    }
}
