//
//  ZoomImageAnimation.swift
//  Digities
//
//  Created by vamsi on 11/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation
import UIKit


struct ImgVariables {
    static var blackBackgroundView = UIView()
    static var statusImageView = UIImageView()
    static var zoomImageView = UIImageView()
    static var startingFrame = CGRect()
    static func removeAllViews(){
        ImgVariables.blackBackgroundView.removeFromSuperview()
        ImgVariables.zoomImageView.removeFromSuperview()
    }
    
}

extension UIImageView{
    
    func zoomAnimation(inViewController controller:UIViewController){
        
        ImgVariables.statusImageView =  self
        if let startingFrame = self.superview?.convert(self.frame, to: nil){
            //statusImageView.alpha = 0
            ImgVariables.blackBackgroundView.frame = controller.view.frame
            ImgVariables.blackBackgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            ImgVariables.blackBackgroundView.alpha = 0
            ImgVariables.blackBackgroundView.isUserInteractionEnabled = true
            ImgVariables.blackBackgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(zoomOut)))
            controller.view.addSubview(ImgVariables.blackBackgroundView)
            let zoomImageView = UIImageView()
            zoomImageView.frame = startingFrame
            ImgVariables.zoomImageView = zoomImageView
            zoomImageView.image = self.image
            zoomImageView.clipsToBounds = true
            zoomImageView.contentMode = .scaleToFill
            zoomImageView.isUserInteractionEnabled = true
            zoomImageView.layer.shadowColor = UIColor.red.cgColor
            zoomImageView.layer.shadowOffset = CGSize(width: 10, height: 10)
            zoomImageView.layer.shadowRadius = 10
            zoomImageView.layer.cornerRadius = 10
            zoomImageView.layer.shadowPath = UIBezierPath(rect: controller.view.bounds).cgPath
            zoomImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(zoomOut)))
            controller.view.addSubview(zoomImageView)
            UIView.animate(withDuration: 0.50) {
                //                let height = (controller.view.frame.width / startingFrame.width) * startingFrame.height
                let height = controller.view.frame.height
                zoomImageView.transform = CGAffineTransform(translationX: 50, y: 200)
                zoomImageView.frame = CGRect(x: 20, y: height/4, width: controller.view.frame.width - 40, height: height/2 - 60)
                //  zoomImageView.transform = CGAffineTransform(scaleX: 10.5, y: 20.5)
                // zoomImageView.frame = CGRect(x: 0, y: y, width: self.view.frame.width, height: height)
                ImgVariables.blackBackgroundView.alpha = 1
            }
        }
    }
    
    
    @objc func zoomOut(){
        if let startingFrame = self.superview?.convert(self.frame, to: nil){
            UIView.animate(withDuration: 0.50, animations: {
                ImgVariables.zoomImageView.frame = startingFrame
                ImgVariables.startingFrame = startingFrame
                ImgVariables.blackBackgroundView.alpha = 0
            }) { (_) in
                //                variables.zoomImageView.transform = .identity
                ImgVariables.zoomImageView.removeFromSuperview()
                ImgVariables.blackBackgroundView.removeFromSuperview()
                ImgVariables.statusImageView.alpha = 1
            }
        }
    }
}
