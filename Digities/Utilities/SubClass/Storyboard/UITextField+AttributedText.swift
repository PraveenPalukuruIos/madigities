//
//  UITextField+AttributedText.swift
//  FirebaseChatApp
//
//  Created by vamsi on 14/04/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}


func SearchBarDesign(searchBar:UISearchBar){
    
    searchBar.backgroundImage = UIImage()
    if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
        textfield.textColor = #colorLiteral(red: 0.1137254902, green: 0.2794491053, blue: 0.2853239477, alpha: 1)
        
        if let backgroundview = textfield.subviews.first {
            backgroundview.backgroundColor = UIColor.white
            backgroundview.layer.cornerRadius = 15;
            backgroundview.clipsToBounds = true
            backgroundview.layer.borderWidth = 0.6
            backgroundview.layer.borderColor = #colorLiteral(red: 0.1137254902, green: 0.2794491053, blue: 0.2853239477, alpha: 1)
        }
    }
    
}





extension UITextField{

    func setAttributedPlaceholder(text:String){
        attributedPlaceholder = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor :UIColor(displayP3Red: 29/255, green: 71/255, blue: 73/255, alpha: 1.0),NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 14)])
    }

    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:
            CGRect(x: 10, y: 5, width: 20, height: 20))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 20, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        rightView = iconContainerView
        rightViewMode = .always
    }




    }


extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}



@IBDesignable
class RoundUIView: UIView {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
}


@IBDesignable
class RoundUIBUTTON: UIButton {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
}


@IBDesignable
class RoundUILabel: UILabel {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
}
