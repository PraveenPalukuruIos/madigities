//
//  UItableViewCell+Identifier.swift
//  GreenApple
//
//  Created by vamsi on 03/05/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

extension UITableViewCell{
    static var identifier:String{
        return String(describing: self)
    }
}
extension UICollectionViewCell{
    static var identifier:String{
        return String(describing: self)
    }
}
