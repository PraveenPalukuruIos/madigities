//
//  RoundButton.swift
//  FirebaseChatApp
//
//  Created by vamsi on 14/04/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

@IBDesignable class RoundButton: UIButton {

    @IBInspectable var cornerRadius:CGFloat = 0.0{
        didSet{
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0.0{
        didSet{
            layer.borderWidth = borderWidth
        }
    }

    @IBInspectable var borderColor:UIColor = .black{
        didSet{
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var shadowRadius:CGFloat = 0.0{
        didSet{
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity:Float = 0.0{
        didSet{
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var shadowOffset:CGSize = CGSize(width: 0, height: 0){
        didSet{
            layer.shadowOffset = shadowOffset
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRoundButton()
    }

     required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupRoundButton()
    }
    
    func setupRoundButton(){
        layer.cornerRadius = frame.size.height/2
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 5
        layer.shadowOpacity = 1.0
        layer.shadowOffset = CGSize(width: 5, height: 5)

    }
    
}

