//
//  File.swift
//  FirebaseChatApp
//
//  Created by vamsi on 14/04/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
typealias okCallBack = () -> ()


struct BaseUrl {
    static let baseUrl = "http://projectdemo.co.in/expensetracking/Androidapi/"
    
    static let getPDFURL = "http://projectdemo.co.in/expensetracking/assets/uploads/expense/bills/"
    static let checkuser = baseUrl + "checkusermail"
    static let login = baseUrl + "user_login"
    static let getUnreadMessages = baseUrl + "get_unread_msg_count/\(Global.compID)/\(Global.ID)"
    static let getNotifications = baseUrl + "notification/\(Global.ID)/\(Global.compID)"
    static let getEmployee = baseUrl + "get_employee_by_company_id/\(Global.compID)"
    static let getAllDraftClaims = baseUrl + "getallclaim/\(Global.empID)/1"
    static let getAllPendingClaims = baseUrl + "getallclaim/\(Global.empID)/2"
    static let getAllRejectedClaims = baseUrl + "getallclaim/\(Global.empID)/4"
    static let getAllApprovedClaims = baseUrl + "getallclaim/\(Global.empID)/3"
    static let getAllProcessedClaims = baseUrl + "getallclaim/\(Global.empID)/5"
    static let getViewAllClaims = baseUrl + "getallclaim/\(Global.empID)/0"
    static let getAllExpenses = baseUrl + "getallexpense_byclaimid/"
    static let getCurrencyDetails = baseUrl + "get_currency_details/"
    static let getEngagementPreference = baseUrl + "get_engagement_preference/\(Global.compID)/"
    static let getExpenseById = baseUrl + "getexpense_byid/"
    static let getClaimBillByTranId = baseUrl + "getclaimbill_by_transid/"
    static let getImagePath = " http://projectdemo.co.in/expensetracking/assets/uploads/expense/bills/"
    static let getPolicyDays = baseUrl + "get_policydays/\(Global.compID)"
    static let getclaimbyid = baseUrl + "getclaimbyid/\(Global.compID)"
    static let clientDropDown = "http://projectdemo.co.in/expensetracking/Androidapi/get_client_details/\(Global.compID)/"
    
    static let DivisionDropDown = "http://projectdemo.co.in/expensetracking/Androidapi/get_division_details/\(Global.compID)/"
    
    static let  engagementDropDown = "http://projectdemo.co.in/expensetracking/Androidapi/get_engagement_code_details/"
    static let  addClaim = "http://projectdemo.co.in/expensetracking/Androidapi/add_claim"
}



let appGreenBackground = UIColor(red: 38/255, green: 89/255, blue: 90/255, alpha: 1)
let appDelegate = UIApplication.shared.delegate as! AppDelegate

func alert(alertTitle:String,
           message:String,
           actionTitle:String,
           viewcontroller:UIViewController,
           OkAction:@escaping okCallBack = {})
{
    let alert = UIAlertController(title: alertTitle, message: message, preferredStyle: .alert)
    let okaction = UIAlertAction(title: actionTitle, style: .default) { (_) in
        OkAction()
    }
    let cancelaction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
    }
    alert.addAction(okaction)
    alert.addAction(cancelaction)
    viewcontroller.present(alert, animated: true, completion: nil)
}
//MARK: - Loader
func showLoader(withMessage message: String = "", color: UIColor = UIColor.black
    ) -> Void {
    let activityContainer: UIView = UIView(frame: UIScreen.main.bounds)
    // activityContainer.backgroundColor = .primary
    let indictorSize = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
    let activityIndicatorView = NVActivityIndicatorView(frame: indictorSize, type: NVActivityIndicatorType.ballRotateChase, color: color, padding: 0)
    activityContainer.tag = 601601
    activityIndicatorView.center = activityContainer.center
    activityIndicatorView.startAnimating()
    activityContainer.addSubview(activityIndicatorView)
    UIApplication.shared.keyWindow!.addSubview(activityContainer)
    UIApplication.shared.keyWindow!.bringSubviewToFront(activityContainer)
}

func hideLoader() -> Void {
    for item in UIApplication.shared.keyWindow!.subviews
        where item.tag == 601601 {
            item.removeFromSuperview()
    }
}



func setRootViewController(viewController: UIViewController) {
    let navController = UINavigationController(rootViewController: viewController)
    navController.isNavigationBarHidden = true
    if appDelegate.window == nil {
        //Create New Window here
        appDelegate.window = UIWindow.init(frame: UIScreen.main.bounds)
    }
    appDelegate.window?.makeKeyAndVisible()
    UIView.transition(with: appDelegate.window!, duration: 0.3, options: .transitionCrossDissolve, animations: {
        appDelegate.window?.rootViewController = navController
    }, completion: { completed in
    })
}

enum DataNotFound:Error {
    case dataNotFound
}

func getUserDetails() -> User? {
    if let data = UserDefaults.standard.value(forKey: "UserDetails") as? Data{
        let userDetails = try? PropertyListDecoder().decode(User.self, from: data)
        return userDetails!
    }
    return nil
}


let defaults = UserDefaults.standard

struct Global {
    static var userName:String{
        if let name = defaults.value(forKey: "name") as? String{
            return name
        }else{
            return ""
        }
    }
    
    static  var compID:String{
        if let id = defaults.value(forKey: "compId") as? String{
            return id
        }else{
            return ""
        }
    }
    
    static  var empID:String{
        if let id = defaults.value(forKey: "empId") as? String{
            return id
        }else{
            return ""
        }
    }
    
    static  var ID:String{
        if let id = defaults.value(forKey: "ID") as? String{
            return id
        }else{
            return ""
        }
    }
    
   static let foodTypeExpense = "food"
   static let stayTypeExpense = "stay"
   static let travelTypeExpense = "travel"
    
}
