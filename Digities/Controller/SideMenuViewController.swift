//
//  SideMenuViewController.swift
//  Digities
//
//  Created by vamsi on 08/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    
    var homeDetails = [HomeModel]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeDetails = GetHomeModel.getSideMenuDetails()
    }

}

extension SideMenuViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeDetails.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuTableViewCell.identifier, for: indexPath) as! SideMenuTableViewCell
        cell.configureCell(sideMenuDetails: homeDetails[indexPath.row])
        return cell
    }
}

extension SideMenuViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.reloadData()
        let title = homeDetails[indexPath.row].title
        switch title {
        case .create:
            moveToCreateClaimVC(with: .create)
        case .draft:
            moveToViewAllClaims(with: .draft)
        case .rejected:
            moveToViewAllClaims(with: .rejected)
        case .processed:
            moveToViewAllClaims(with: .processed)
        case .address:
            moveToAddressVC()
        case .pending:
            moveToViewAllClaims(with: .pending)
        case .approved:
            moveToViewAllClaims(with: .approved)
        case .viewAllClaims:
            moveToViewAllClaims(with: .viewAllClaims)
        case .signout:
            let signInVC:SignInViewController = UIStoryboard(storyboard: .Main).controller()
            removeUserDefaults()
            setRootViewController(viewController: signInVC)
        }
    }
    
    func removeUserDefaults(){
        defaults.setValue(nil, forKey: "name")
        defaults.setValue(nil, forKey: "compId")
        defaults.setValue(nil, forKey: "empId")
        defaults.setValue(nil, forKey: "ID")
    }
    
    func moveToCreateClaimVC(with title:TitleType){
        let revealVC:SWRevealViewController = self.revealViewController()
        let createClaimVC:CreateClaimViewController = UIStoryboard(storyboard: .CreateClaim).controller()
        createClaimVC.isFrom = title
        createClaimVC.isFromSideMenu = true
        let newFrontVC = UINavigationController.init(rootViewController: createClaimVC)
        newFrontVC.navigationBar.barTintColor = appGreenBackground
        revealVC.pushFrontViewController(newFrontVC, animated: true)
    }
    
    func moveToViewAllClaims(with title:TitleType){
        let revealVC:SWRevealViewController = self.revealViewController()
        let viewAllClaimVC:ViewAllClaimsViewController = UIStoryboard(storyboard: .ViewAllClaims).controller()
        viewAllClaimVC.isFrom = title
        viewAllClaimVC.isFromSideMenu = true
        let newFrontVC = UINavigationController.init(rootViewController: viewAllClaimVC)
        newFrontVC.navigationBar.barTintColor = appGreenBackground
        revealVC.pushFrontViewController(newFrontVC, animated: true)
    }
    
    func moveToAddressVC(){
        let revealVC:SWRevealViewController = self.revealViewController()
        let addressBookVC:AddressBookViewController = UIStoryboard(storyboard: .Home).controller()
        let newFrontVC = UINavigationController.init(rootViewController: addressBookVC)
        addressBookVC.isFromSideMenu = true
        newFrontVC.navigationBar.barTintColor = appGreenBackground
        revealVC.pushFrontViewController(newFrontVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
