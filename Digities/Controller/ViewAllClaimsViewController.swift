//
//  ViewAllClaimsViewController.swift
//  Digities
//
//  Created by vamsi on 08/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class ViewAllClaimsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var topTitle: UILabel!
    
    
    var claimsArray = [ViewAllClaimsDetails]()
    var filteredClaims = [ViewAllClaimsDetails]()
    var isFiltered = false
    var isFrom:TitleType?
    var totalAmount = 0
    var companyId = 98
    var currency = ""
    var engagementCode = ""
    var isFromSideMenu = false
    let alertLabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        alertLabel.isHidden = true
        alertLabel.frame = CGRect(x: view.frame.width/2 - 70, y: view.frame.height/2, width: view.frame.width, height: 50)
        view.addSubview(alertLabel)
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false

        tableView.addGestureRecognizer(tap)
        
        SearchBarDesign(searchBar:searchBar)

        
        setupUI()
        
        getTitleAndHitAPI()
    }
    
    @objc func dismissKeyboard() {
     
        tableView.endEditing(true)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.endEditing(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    
    func setupUI(){
        getCurrencyDetails_API { [unowned self] (resp) in
            print(resp)
            if let currency = resp.currency{
                defaults.set(currency, forKey: "currency")
                self.currency = resp.symbol ?? ""
            }
        }
        getEngagementDetails_API { [unowned self] (engagement) in
            self.engagementCode = engagement.eng_preference ?? ""
        }
        welcomeLbl.text = "Welcome \(Global.userName)"
        tableView.estimatedRowHeight = 230
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = .init(frame: .zero)
    }
    
    
    @IBAction func backBtn(_ sender: Any) {
        if !isFromSideMenu{
            navigationController?.popViewController(animated: true)
            return
        }
        let swVC:SWRevealViewController =  UIStoryboard(storyboard: .Home).controller()
        setRootViewController(viewController: swVC)
    }
    
    func getTitleAndHitAPI(){
        guard let title = isFrom else{return}
        var url = ""
        switch title {
        case .draft:
            url = BaseUrl.getAllDraftClaims
        case .pending:
            url = BaseUrl.getAllPendingClaims
        case .rejected:
            url = BaseUrl.getAllRejectedClaims
        case .approved:
            url = BaseUrl.getAllApprovedClaims
        case .processed:
            url = BaseUrl.getAllProcessedClaims
        case .viewAllClaims:
            url = BaseUrl.getViewAllClaims
        default:
            return
        }
        titleLabel.text = title.self.rawValue
        topTitle.text = title.self.rawValue
        
        getAllClaims_API(url: url) { [unowned self] (records) in
            self.claimsArray.append(contentsOf: records)
            self.setUpTotalAmount()
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
    }
    
    func setUpTotalAmount(){
        guard let title = isFrom else{return}
        var customString = ""
        switch title {
        case .draft:
            customString = "Total Reimb.Amount : "
            self.claimsArray.forEach({ (details) in
                if let amount =  details.total_reimburse_amt{
                    if let totalAmnt = Int(amount){
                        self.totalAmount += totalAmnt
                    }
                }
            })
        case .pending:
            customString = "Total Pending Amount : "
            self.claimsArray.forEach({ (details) in
                if let amount =  details.total_reimburse_amt{
                    if let totalAmnt = Int(amount){
                        self.totalAmount += totalAmnt
                    }
                }
            })
        case .rejected:
            customString = "Total Rejected Amount : "
            self.claimsArray.forEach({ (details) in
                if let amount =  details.total_rejected_amt{
                    if let totalAmnt = Int(amount){
                        self.totalAmount += totalAmnt
                    }
                }
            })
        case .approved:
            customString = "Total Approved Amount : "
            self.claimsArray.forEach({ (details) in
                if let amount =  details.total_approved_amt{
                    if let totalAmnt = Int(amount){
                        self.totalAmount += totalAmnt
                    }
                }
            })
        case .processed:
            customString = "Total Paid Amount : "
            self.claimsArray.forEach({ (details) in
                if let amount =  details.total_paid_amt{
                    if let totalAmnt = Int(amount){
                        self.totalAmount += totalAmnt
                    }
                }
            })
        case .viewAllClaims:
            customString = "Total Amount : "
            self.claimsArray.forEach({ (details) in
                if let amount =  details.total_reimburse_amt{
                    if let totalAmnt = Int(amount){
                        self.totalAmount += totalAmnt
                    }
                }
            })
        default:
            return
        }
        
        DispatchQueue.main.async {
            self.totalAmountLabel.text = "\(customString) \(self.currency)\(self.totalAmount)"
        }
    }
}

extension ViewAllClaimsViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltered ? filteredClaims.count : claimsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ViewAllClaimsTableViewCell.identifier, for: indexPath) as! ViewAllClaimsTableViewCell
        let claimDetails = isFiltered ? filteredClaims : claimsArray
        cell.configureCell(claimDetails: claimDetails[indexPath.row], title: isFrom,currency: currency,engagement:engagementCode)
        return cell
    }
}

extension ViewAllClaimsViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let claimDetails = isFiltered ? filteredClaims : claimsArray
        gotoDetailClaimVC(claimDetails: claimDetails[indexPath.row], title: isFrom)
        print(indexPath.row)
    }
    
    func gotoDetailClaimVC(claimDetails:ViewAllClaimsDetails,title:TitleType?){
        let detailClaimVC:DetailClaimViewController = UIStoryboard(storyboard: .DetailClaim).controller()
        detailClaimVC.claimDetails = claimDetails
        detailClaimVC.isFrom = title
        navigationController?.pushViewController(detailClaimVC, animated: true)
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}




extension ViewAllClaimsViewController{
    
    func getAllClaims_API(url:String,completion:@escaping([ViewAllClaimsDetails])->()){
        showLoader()
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                let claims = try JSONDecoder().decode(ViewAllClaims.self, from: response)
                if let issuccess = claims.status{
                    if issuccess == "success"{
                        if let allClaimsData = claims.data{
                            completion(allClaimsData)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.alertLabel.isHidden = false
                            self.alertLabel.text = "No Claims Found"
                        }
                    }
                   
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                    alert(alertTitle: "Alert !!!", message: "Getting claim details failed", actionTitle: "Ok", viewcontroller: self)
                }
                print(DataNotFound.dataNotFound)
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
        
    }
}

extension ViewAllClaimsViewController{
    
    func getCurrencyDetails_API(completion:@escaping(CurrencyData)->()){
        showLoader()
        let url = BaseUrl.getCurrencyDetails + "\(companyId)"
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                let currency = try JSONDecoder().decode(CurrencyHead.self, from: response)
                if let currencyData = currency.currency{
                    completion(currencyData)
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                    alert(alertTitle: "Alert !!!", message: "Getting claim details failed", actionTitle: "Ok", viewcontroller: self)
                }
                print(DataNotFound.dataNotFound)
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
        
    }
    
    func getEngagementDetails_API(completion:@escaping(EngagementData)->()){
        showLoader()
        let url = BaseUrl.getEngagementPreference + "\(companyId)"
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                let engagement = try JSONDecoder().decode(EngagementHead.self, from: response)
                if let engagementData = engagement.preference{
                    completion(engagementData[0])
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                    alert(alertTitle: "Alert !!!", message: "Getting claim details failed", actionTitle: "Ok", viewcontroller: self)
                }
                print(DataNotFound.dataNotFound)
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
        
    }    
}

extension ViewAllClaimsViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty{
            isFiltered = true
            filteredClaims = claimsArray.filter({($0.client_name?.lowercased().contains(searchText.lowercased()))! || ($0.claim_date?.lowercased().contains(searchText.lowercased()))!  || ($0.division_name?.lowercased().contains(searchText.lowercased()))! || ($0.engagement_code?.lowercased().contains(searchText.lowercased()))! || ($0.total_expense_amt?.lowercased().contains(searchText.lowercased()))! || ($0.total_reimburse_amt?.lowercased().contains(searchText.lowercased()))! })
            tableView.reloadData()
        }else{
            isFiltered = false
            tableView.reloadData()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
}
