//
//  AddressBookViewController.swift
//  Digities
//
//  Created by vamsi on 05/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class AddressBookViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var allEmployesLbl: UILabel!
    
    var employee = [EmployeeData]()
    var filteredEmployeeData = [EmployeeData]()
    var isFiltered = false
    var isFromSideMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        SearchBarDesign(searchBar:searchBar)
        
        welcomeLabel.text = "Welcome \(Global.userName)"
        getEmployeeDetails_API { (records) in
            self.employee.append(contentsOf: records)
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.allEmployesLbl.text = "All Employees"
                self.titleLabel.text = "All Employees"
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.endEditing(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ImgVariables.removeAllViews()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        if !isFromSideMenu{
        navigationController?.popViewController(animated: true)
            return
        }
        let swVC:SWRevealViewController =  UIStoryboard(storyboard: .Home).controller()
        setRootViewController(viewController: swVC)
    }
}

extension AddressBookViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltered ? filteredEmployeeData.count : employee.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AddressTableViewCell.identifier, for: indexPath) as! AddressTableViewCell
       let empDetails = isFiltered ? filteredEmployeeData : employee
        cell.vc = self
        cell.configureCell(with: empDetails[indexPath.row])
        cell.callBack = { phoneNumber in
            if let url = URL(string: "tel://\(phoneNumber)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        return cell
    }
}

extension AddressBookViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
}

extension AddressBookViewController{
    
    func getEmployeeDetails_API(completion:@escaping([EmployeeData])->()){
        showLoader()
        NetworkManager.getReponse(url: BaseUrl.getEmployee, success: { (response) in
            do{
                let employee = try JSONDecoder().decode(Employee.self, from: response)
                if let employeeData = employee.employee{
                    completion(employeeData)
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                    alert(alertTitle: "Alert !!!", message: "Getting employess details failed", actionTitle: "Ok", viewcontroller: self)
                }
                print(DataNotFound.dataNotFound)
            }
        }) { (err) in
            hideLoader()
            print(err)
        }

        
        
        
        
//        NetworkManager.afgetRequest(url: BaseUrl.getEmployee, paramaters: [:], success: { (response) in
//            do{
//                let employee = try JSONDecoder().decode(Employee.self, from: response)
//                if let employeeData = employee.employee{
//                    completion(employeeData)
//                }
//                DispatchQueue.main.async {
//                    hideLoader()
//                }
//            }catch{
//                DispatchQueue.main.async {
//                    hideLoader()
//                    alert(alertTitle: "Alert !!!", message: "Getting employess details failed", actionTitle: "Ok", viewcontroller: self)
//                }
//                print(DataNotFound.dataNotFound)
//            }
//        }) { (err) in
//            hideLoader()
//            print(err)
//        }
       
    }
    
   
    
    
}

extension AddressBookViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty{
        isFiltered = true
        filteredEmployeeData = employee.filter({($0.name?.capitalized.contains(searchText.capitalized))!
            || ($0.phone_number?.capitalized.contains(searchText.capitalized))!
        })
            tableView.reloadData()
        }else{
            isFiltered = false
            tableView.reloadData()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }

    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    
    
}
