//
//  ExpenseDetailsViewController.swift
//  Digities
//
//  Created by Santhana on 29/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos
import BSImagePicker
import WebKit

class ExpenseDetailsViewController: UIViewController {
    
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var expenceTabelView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var webContainderView: UIView!
    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet weak var closeButton: UIButton!
    
    var detailClaimData:DetailClaimsData?
    var isFrom:TitleType?
    
    var expenseDetails:ExpenseData?
    var claimBillDetails: [ClaimBillData]?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        getDataFromServer()
        setupUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ImgVariables.removeAllViews()
        view.endEditing(true)

    }
    func setupUI(){
        userNameLabel.text = "Welcome \(Global.userName)"
        expenceTabelView.estimatedRowHeight = 230
        expenceTabelView.rowHeight = UITableView.automaticDimension
        expenceTabelView.tableFooterView = .init(frame: .zero)
    }
    
    func showPDF(path: String){
        let url: URL! = URL(string: path )
        webView.load(URLRequest(url: url))
        webContainderView.isHidden = false
        webView.isHidden = false
        closeButton.isHidden = false
    }
    
    
    @IBAction func onBackButton(_ sender: Any) {
        
        if(webContainderView.isHidden){
          self.navigationController?.popViewController(animated: true)
        }else {
            webContainderView.isHidden = true
            webView.isHidden = true
            closeButton.isHidden = true
        }
        
    }
    
    @IBAction func onPDFViewClose(_ sender: Any) {
        
        webContainderView.isHidden = true
        webView.isHidden = true
        closeButton.isHidden = true
    }
    
    
    
    
    func getDataFromServer(){
        getExpenseDetails_API { (expData) in
            self.expenseDetails = expData
            DispatchQueue.main.async {
                self.expenceTabelView.reloadData()
            }
        }
        
        getClaimBillDetails_API { (claimData) in
            self.claimBillDetails = claimData.expenseimage
            DispatchQueue.main.async {
                self.expenceTabelView.reloadData()
            }
        }
        
    }


}

extension ExpenseDetailsViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ExpenseDetailsTableViewCell.identifier, for: indexPath) as! ExpenseDetailsTableViewCell
        if let expDetails = expenseDetails{
            cell.configureCell(expDetails: expDetails, title: isFrom)
        }
        if let claimDetails = claimBillDetails{
            cell.setupClaimBill(claimDetails: claimDetails)
        }
        
        cell.vc = self
        
        return cell
    }
    
}



extension ExpenseDetailsViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 650
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
extension ExpenseDetailsViewController{
    
    func getExpenseDetails_API(completion:@escaping(ExpenseData)->()){
        showLoader()
        var tranId = ""
        if let data = detailClaimData{
            if let id = data.trans_id{
                tranId = id
            }
        }
        let url = BaseUrl.getExpenseById + tranId
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                let exp = try JSONDecoder().decode(ExpenseDetailsHead.self, from: response)
                if let expenseData = exp.expensedata{
                    completion(expenseData)
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                }
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
    }
}

extension ExpenseDetailsViewController{
    
    func getClaimBillDetails_API(completion:@escaping(ClaimBill)->()){
        showLoader()
        var tranId = ""
        if let data = detailClaimData{
            if let id = data.trans_id{
                tranId = id
            }
        }
        let url = BaseUrl.getClaimBillByTranId + tranId
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                let resp = try JSONDecoder().decode(ClaimBill.self, from: response)
                if let claimBill = resp.expenseimage{
                    completion(resp)
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                }
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
    }
}
