//
//  AllMessagesViewController.swift
//  Digities
//
//  Created by vamsi on 14/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class AllMessagesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var notificationsArr = [AllNotificationsData]()
    var filterednotificationsArr = [AllNotificationsData]()
    var isFiltered = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        
        hideKeyboardWhenTappedAround()
        SearchBarDesign(searchBar: searchBar)
        setupUI()
    }
    
    
    func setupUI(){
        getAllNotifications_API(completion: { (notifs) in
            self.notificationsArr.append(contentsOf: notifs)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) {
            print("no notifications found")
        }
        welcomeLbl.text = "Welcome \(Global.userName)"
        tableView.estimatedRowHeight = 230
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = .init(frame: .zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension AllMessagesViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltered ? filterednotificationsArr.count : notificationsArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AllMessagesTableViewCell.identifier, for: indexPath) as! AllMessagesTableViewCell
        let notifDetails = isFiltered ? filterednotificationsArr : notificationsArr
        cell.configureCell(notifDetails[indexPath.row])
        return cell
    }
}

extension AllMessagesViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


extension AllMessagesViewController{
    
    func getAllNotifications_API(completion:@escaping([AllNotificationsData])->(),failure:@escaping()->()){
        showLoader()
        
        NetworkManager.getReponse(url: BaseUrl.getNotifications, success: { (response) in
            do{
                let resp = try JSONDecoder().decode(AllNotificationsHead.self, from: response)
                if let messages = resp.messages{
                    completion(messages)
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    failure()
                    hideLoader()
                }
            }
        }) { (err) in
            hideLoader()
            failure()
            print(err)
        }
        
    }
}
extension AllMessagesViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty{
            isFiltered = true
            filterednotificationsArr = notificationsArr.filter({($0.subject?.lowercased().contains(searchText.lowercased()))! || ($0.message?.lowercased().contains(searchText.lowercased()))! })
            tableView.reloadData()
        }else{
            isFiltered = false
            tableView.reloadData()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
}
