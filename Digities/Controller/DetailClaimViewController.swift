//
//  DetailClaimViewController.swift
//  Digities
//
//  Created by vamsi on 08/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class DetailClaimViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var welcomLbl: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var navTitle: UILabel!
    
    
    
    var claimsArray = [DetailClaimsData]()
    var filteredClaims = [DetailClaimsData]()
    var isFiltered = false
    var isFrom:TitleType?
    var claimDetails:ViewAllClaimsDetails?
    let noExpenseLabel = UILabel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        
        tableView.addGestureRecognizer(tap)
        
        searchBar.delegate = self
        SearchBarDesign(searchBar:searchBar)

        getTitleAndHitAPI()
    }
    @objc func dismissKeyboard() {
        
        tableView.endEditing(true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupUI(){
        noExpenseLabel.isHidden = true
        noExpenseLabel.frame = CGRect(x: view.frame.width/2 - 70, y: view.frame.height/2, width: view.frame.width, height: 50)
        noExpenseLabel.text = "No Expenses Found"
        view.addSubview(noExpenseLabel)
        welcomLbl.text = "Welcome \(Global.userName)"
        tableView.estimatedRowHeight = 230
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = .init(frame: .zero)
    }
    
    
    func getTitleAndHitAPI(){
        navTitle.text = "All Expenses"
        titleLabel.text = "All Expenses"
        var claimId = ""
        if let claimData = claimDetails{
            if let id = claimData.id{
                claimId = id
            }
        }
        let url = BaseUrl.getAllExpenses + claimId
        getAllClaims_API(url: url, completion: { (records) in
            self.claimsArray.append(contentsOf: records)
            DispatchQueue.main.async {
                self.noExpenseLabel.isHidden = true
                self.tableView.reloadData()
            }
        }) {
            DispatchQueue.main.async {
                self.noExpenseLabel.isHidden = false
            }
        }
        
    }
    
}

extension DetailClaimViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltered ? filteredClaims.count : claimsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DetailClaimTableViewCell.identifier, for: indexPath) as! DetailClaimTableViewCell
        let claimDetails = isFiltered ? filteredClaims : claimsArray
        cell.configureCell(claimDetails: claimDetails[indexPath.row], title: isFrom)
        return cell
    }
}

extension DetailClaimViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let claimDetails = isFiltered ? filteredClaims : claimsArray
        gotoMoreDetailClaimVC(claimDetails: claimDetails[indexPath.row], title: isFrom)
        
    }
    func gotoMoreDetailClaimVC(claimDetails:DetailClaimsData,title:TitleType?){
        let moreDetailClaimVC:ExpenseDetailsViewController = UIStoryboard(storyboard: .DetailClaim).controller()
        moreDetailClaimVC.detailClaimData = claimDetails
        moreDetailClaimVC.isFrom = title
        navigationController?.pushViewController(moreDetailClaimVC, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


extension DetailClaimViewController{
    
    func getAllClaims_API(url:String,completion:@escaping([DetailClaimsData])->(),failure:@escaping()->()){
        showLoader()
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                let claims = try JSONDecoder().decode(DetailClaims.self, from: response)
                if let allClaimsData = claims.allexpense{
                    completion(allClaimsData)
                }
                DispatchQueue.main.async {
                    if self.claimsArray.count == 0{
                        failure()
                    }
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    failure()
                    hideLoader()
                }
            }
        }) { (err) in
            hideLoader()
            failure()
            print(err)
        }
        
    }
}
extension DetailClaimViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty{
            isFiltered = true
            filteredClaims = claimsArray.filter({($0.exp_cat_name?.lowercased().contains(searchText.lowercased()))! || ($0.exp_amount?.lowercased().contains(searchText.lowercased()))! || ($0.expense_date?.lowercased().contains(searchText.lowercased()))!  })
            tableView.reloadData()
        }else{
            isFiltered = false
            tableView.reloadData()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
}
