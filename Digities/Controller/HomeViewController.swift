//
//  HomeViewController.swift
//  Digities
//
//  Created by vamsi on 04/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit
import Toast_Swift

class HomeViewController: UIViewController {

    @IBOutlet weak var rightBarButton: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var openButton: UIBarButtonItem!

    @IBOutlet weak var bellIcon: UIBarButtonItem!
    var homeDetails = [HomeModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        welcomeLabel.text = "Welcome \(Global.userName)"
        setUpRevealingButton()
       homeDetails = GetHomeModel.getHomeDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        getUnreadMessages_API { (count) in
            DispatchQueue.main.async {
                if count > 0{
                self.rightBarButton.addBadge(number: count)
                }
            }
        }

    }
    
    @IBAction func belllIcon(_ sender: UIBarButtonItem) {
        let allMessageVC:AllMessagesViewController = UIStoryboard(storyboard: .Home).controller()
        self.navigationController?.pushViewController(allMessageVC, animated: true)
    }
    
    func setUpRevealingButton()
    {
        openButton.target = revealViewController()
        openButton.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(revealViewController().tapGestureRecognizer())
        let width = view.frame.width
        self.revealViewController().rearViewRevealWidth = width - (width/4.25)
    }

}

extension HomeViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.identifier, for: indexPath) as! HomeCollectionViewCell
        cell.configureCell(homeDetails[indexPath.row])
        return cell
    }
}
extension HomeViewController:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let title = homeDetails[indexPath.row].title
        switch title {
        case .create:
            moveToCreateClaimsVC(with: .create)
        case .address:
            moveToAddressVC()
        case .draft:
            moveToViewAllClaimsVC(with: .draft)
        case .pending:
            moveToViewAllClaimsVC(with: .pending)
        case .rejected:
            moveToViewAllClaimsVC(with: .rejected)
        case .approved:
            moveToViewAllClaimsVC(with: .approved)
        case .processed:
            moveToViewAllClaimsVC(with: .processed)
        case .viewAllClaims:
            moveToViewAllClaimsVC(with: .viewAllClaims)
        case .signout:
            return
        }
    }
    
    func moveToCreateClaimsVC(with title:TitleType){
        let createClaimVC:CreateClaimViewController = UIStoryboard(storyboard: .CreateClaim).controller()
        createClaimVC.isFrom = title
        navigationController?.pushViewController(createClaimVC, animated: true)
    }
    
    func moveToViewAllClaimsVC(with title:TitleType){
        let viewAllClaimVC:ViewAllClaimsViewController = UIStoryboard(storyboard: .ViewAllClaims).controller()
        viewAllClaimVC.isFrom = title
        navigationController?.pushViewController(viewAllClaimVC, animated: true)
    }
    
    func moveToAddressVC(){
        let adressBookVC:AddressBookViewController = UIStoryboard(storyboard: .Home).controller()
        navigationController?.pushViewController(adressBookVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let frame = self.collectionView.frame.width / 2 - 10
        let heght = self.collectionView.frame.width / 2 - 10
        
        return CGSize(width: frame, height: heght)
    }
    
    
    
}
extension HomeCollectionViewCell:UICollectionViewDelegateFlowLayout{
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }
////
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//
//        let numOfColumns : CGFloat = 2
//        let cellDimension = (collectionView.frame.width/numOfColumns)
//        return CGSize(width: cellDimension, height: cellDimension)
//    }
}


extension HomeViewController{
    func getUnreadMessages_API(completion:@escaping(Int)->()){
        showLoader()
        let url = BaseUrl.getUnreadMessages
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                if let resp =  try JSONSerialization.jsonObject(with: response, options: .mutableLeaves) as? [String:Any]{
                    if let count = resp["msgcount"] as? Int{
                        completion(count)
                    }
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                }
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
        
    }
}

extension CAShapeLayer {
     func drawCircleAtLocation(location: CGPoint, withRadius radius: CGFloat, andColor color: UIColor, filled: Bool) {
        fillColor = filled ? color.cgColor : UIColor.white.cgColor
        strokeColor = color.cgColor
        let origin = CGPoint(x: location.x - radius, y: location.y - radius)
        path = UIBezierPath(ovalIn: CGRect(origin: origin, size: CGSize(width: radius * 2, height: radius * 2))).cgPath
    }
}
private var handle: UInt8 = 0

extension UIBarButtonItem {
    private var badgeLayer: CAShapeLayer? {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as? CAShapeLayer
        } else {
            return nil
        }
    }
    
    func addBadge(number: Int, withOffset offset: CGPoint = CGPoint.zero, andColor color: UIColor = UIColor.red, andFilled filled: Bool = true) {
        guard let view = self.value(forKey: "view") as? UIView else { return }
        
        badgeLayer?.removeFromSuperlayer()
        
        // Initialize Badge
        let badge = CAShapeLayer()
        let radius = CGFloat(9)
        let location = CGPoint(x: view.frame.width - (radius + offset.x) - 20, y: (radius + offset.y))
        badge.drawCircleAtLocation(location: location, withRadius: radius, andColor: color, filled: filled)
        view.layer.addSublayer(badge)
        
        // Initialiaze Badge's label
        let label = CATextLayer()
        label.string = "\(number)"
        label.alignmentMode = CATextLayerAlignmentMode.center
        label.fontSize = 11
        label.frame = CGRect(origin: CGPoint(x: location.x - 8, y: offset.y), size: CGSize(width: 15, height: 18))
        label.foregroundColor = filled ? UIColor.white.cgColor : color.cgColor
        label.backgroundColor = UIColor.clear.cgColor
        label.contentsScale = UIScreen.main.scale
        badge.addSublayer(label)
        
        // Save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, badge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func updateBadge(number: Int) {
        if let text = badgeLayer?.sublayers?.filter({ $0 is CATextLayer }).first as? CATextLayer {
            text.string = "\(number)"
        }
    }
    
    func removeBadge() {
        badgeLayer?.removeFromSuperlayer()
    }
}
