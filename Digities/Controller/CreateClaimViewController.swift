//
//  CreateClaimViewController.swift
//  Digities
//
//  Created by vamsi on 08/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit
import iOSDropDown
import Toast_Swift
class CreateClaimViewController: UIViewController {
    
    @IBOutlet weak var welcomLbl: UILabel!

    @IBOutlet weak var clientName: UITextField!
    @IBOutlet weak var divisionName: UITextField!
    @IBOutlet weak var engagementCodeTF: UITextField!
    @IBOutlet weak var claimDate: UITextField!
    @IBOutlet weak var openButton: UIBarButtonItem!
    
    
    @IBOutlet weak var ClientTable: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint! // 150
    
    
    @IBOutlet weak var divisionTable: UITableView!
    @IBOutlet weak var divisionHeight: NSLayoutConstraint! // 150
    
    
    @IBOutlet weak var engagementTable: UITableView!
    @IBOutlet weak var engageMenttablelHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var engagementRoundView: RoundUIView!
    
    @IBOutlet weak var engagementRoundViewHeight: NSLayoutConstraint! //85
    
    @IBOutlet weak var engagmentImageView: UIImageView!
    
    @IBOutlet weak var engagementLabel: UILabel!
    @IBOutlet weak var rightBarButton: UIBarButtonItem!

    
    @IBOutlet weak var AlertLbl: UILabel!
    @IBOutlet weak var alertView: RoundUIView!
    

    var clientList = [ClientDetail]()
    var filteredClientList = [ClientDetail]()
    var isClientFiltered = false
    
    var divisionList = [DivisionDetail]()
    var filteredDivisionList = [DivisionDetail]()
    var isDivisionFiltered = false
    
    var engagementList = [EngagementDetail]()
    var filteredEngagementList = [EngagementDetail]()
    var isEngagementFiltered = false

    
    var Clientid = String()
    var divisionid = String()
    var engagugeid = String()
    var engagementPreference = String()


    
    var isFrom:TitleType?
    var isFromSideMenu = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        welcomLbl.text = "Welcome \(Global.userName)"
        self.navigationController?.isNavigationBarHidden = false

        
        self.alertView.isHidden = true
        self.AlertLbl.isHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        
        ClientTable.addGestureRecognizer(tap)
        divisionTable.addGestureRecognizer(tap)
        engagementTable.addGestureRecognizer(tap)

//        hideKeyboardWhenTappedAround()
        
        ClientTable.delegate = self
        ClientTable.dataSource = self
        divisionTable.delegate = self
        divisionTable.dataSource = self
        engagementTable.delegate = self
        engagementTable.dataSource = self
        
        clientName.delegate = self
        divisionName.delegate = self
        engagementCodeTF.delegate = self
        clientName.addTarget(self, action: #selector(CreateClaimViewController.textFieldDidChange), for: .editingChanged)
        divisionName.addTarget(self, action: #selector(CreateClaimViewController.textFieldDidChange), for: .editingChanged)
        engagementCodeTF.addTarget(self, action: #selector(CreateClaimViewController.textFieldDidChange), for: .editingChanged)
        
        ClientTable.isHidden = true
        divisionTable.isHidden = true
        engagementTable.isHidden = true
        
        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd-MM-yyyy"
        let result = formatter.string(from: date)
        
        self.claimDate.text = result

//        SetUpUI()
        setUpRevealingButton()
//        EngagugeMentCode()
        GetEngagementPreference()
        
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        let currentString: String = textField.text!
        
        if textField == clientName {
            ClientTable.isHidden = false
            //getClientListAPI(searchKey: currentString)
            if(!currentString.isEmpty){
                print("entered search")
                isClientFiltered = true
                filteredClientList = clientList.filter({($0.client_name?.lowercased().contains(currentString.lowercased()))!})
            }else{
                print("show default")
                isClientFiltered = false
            }
            Clientid = ""
            ClientTable.reloadData()
            
        }else if textField == divisionName {
            divisionTable.isHidden = false
            //getClientListAPI(searchKey: currentString)
            if(!currentString.isEmpty){
                print("entered search")
                isDivisionFiltered = true
                filteredDivisionList = divisionList.filter({($0.division?.lowercased().contains(currentString.lowercased()))!})
            }else{
                print("show default")
                isDivisionFiltered = false
            }
            divisionid = ""
            divisionTable.reloadData()
            
        }else if textField  ==  engagementCodeTF{
            engagementTable.isHidden = false
            //getClientListAPI(searchKey: currentString)
            if(!currentString.isEmpty){
                print("entered search")
                isEngagementFiltered = true
                filteredEngagementList = engagementList.filter({($0.engagement_code?.lowercased().contains(currentString.lowercased()))!})
            }else{
                print("show default")
                isEngagementFiltered = false
            }
            engagugeid = ""

            engagementTable.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
//        hideKeyboardWhenTappedAround()
        divisionName.resignFirstResponder()
        clientName.resignFirstResponder()
        engagementCodeTF.resignFirstResponder()
        clientName.clearButtonMode = .always
        divisionName.clearButtonMode = .always
        engagementCodeTF.clearButtonMode = .always
        getUnreadMessages_API { (count) in
            DispatchQueue.main.async {
                if count > 0{
                    self.rightBarButton.addBadge(number: count)
                }
            }
        }
    }
    
    @objc func dismissKeyboard() {
        ClientTable.endEditing(true)
        divisionTable.endEditing(true)
        engagementTable.endEditing(true)
    }
    
    @IBAction func belllIcon(_ sender: UIBarButtonItem) {
        let allMessageVC:AllMessagesViewController = UIStoryboard(storyboard: .Home).controller()
        self.navigationController?.pushViewController(allMessageVC, animated: true)
    }

    
    func SetUpUI() {
        clientName.setIcon(UIImage(named: "dropdown")!)
        clientName.setAttributedPlaceholder(text: "Select")
        
        divisionName.setIcon(UIImage(named: "dropdown")!)
        divisionName.setAttributedPlaceholder(text: "Select")
        
        engagementCodeTF.setIcon(UIImage(named: "dropdown")!)
        engagementCodeTF.setAttributedPlaceholder(text: "Select")
        
        claimDate.setIcon(UIImage(named: "dropdown")!)
        claimDate.setAttributedPlaceholder(text: "Select")
        
        
        let imgView = UIImageView(frame: CGRect(x: 50, y: 0, width: view.frame.width/2, height: 40))
        imgView.image = #imageLiteral(resourceName: "topbanner.png")
        imgView.backgroundColor = .clear
        imgView.contentMode = .scaleAspectFit
        navigationItem.titleView = imgView
    }
    
    func setUpRevealingButton()
    {
        openButton.target = revealViewController()
        openButton.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(revealViewController().tapGestureRecognizer())
        let width = view.frame.width
        self.revealViewController().rearViewRevealWidth = width - (width/4.25)    }
    
    
    
    @IBAction func ClientName_Action(_ sender: Any) {
         clientName.becomeFirstResponder()
        
        if clientName.text == "" {
            ClientTable.isHidden = false
            divisionTable.isHidden = true
            engagementTable.isHidden = true
        }else{
            ClientTable.isHidden = true
            divisionTable.isHidden = true
            engagementTable.isHidden = true
        }
        
        
    }
    
    @IBAction func divisionName_Action(_ sender: Any) {
        divisionName.becomeFirstResponder()
        
        if divisionName.text == "" {
            divisionTable.isHidden = false
            ClientTable.isHidden = true
            engagementTable.isHidden = true
        }else{
            divisionTable.isHidden = true
            ClientTable.isHidden = true
            engagementTable.isHidden = true
        }
        
    }
    
    @IBAction func engageMentCode_Action(_ sender: Any) {
        engagementCodeTF.becomeFirstResponder()
        
        if self.engagementCodeTF.text == "" {
            engagementTable.isHidden = false
        }else{
            divisionTable.isHidden = true
            ClientTable.isHidden = true
            engagementTable.isHidden = true
        }
    }
    
    
    @IBAction func Continue_Action(_ sender: Any) {
        
        if (Clientid.isEmpty) {
            self.view.makeToast("Select Client", duration: 2.0, position: .bottom)
        }else if (divisionid.isEmpty) {
            self.view.makeToast("Select Division", duration: 2.0, position: .bottom)
        }else if (engagementPreference == "1" && engagugeid.isEmpty){
            self.view.makeToast("Select Engagement", duration: 2.0, position: .bottom)
        }else if self.claimDate.text != "" {

           // let params1 = [
           //   "comp_id": Global.compID,
           //   "client_id": Clientid,
           //   "client_name": self.clientName.text ?? "",
           //   "division_id":divisionid,
           //   "division_name":self.divisionName.text ?? "",
           ///   "employee_id":engagugeid,
           //   "engagement_code":engagugeid,
           //   "claim_date": self.claimDate.text ?? "",
           //   "claim_status": "1",
           //   "created_by":Global.userName
           //     ] as! [String : String]
            let name = self.clientName.text ?? ""
            
            let params: String  =  "comp_id=\(Global.compID)&client_id=\(Clientid)&client_name=\(name)&division_id=\(divisionid)&division_name=\(self.divisionName.text ?? "")&employee_id=\(Global.empID)&engagement_code=\(engagugeid)&claim_date=\(self.claimDate.text ?? "")&claim_status=\(name)&created_by=\(Global.userName)"
            
            print(params)
            
           // POSTMethodByusingUrlSession(url: "http://projectdemo.co.in/expensetracking/Androidapi/add_claim", Prameters: params1) { (data) in
           //     print(data)
                
               
                
          //  }
            
            createCliam(param: params, success: { (BaseResponse) in
                
                DispatchQueue.main.async {
                   
                    if let vC = UIStoryboard(name: "CreateClaim", bundle: nil).instantiateViewController(withIdentifier: "DetailedCreateClaim") as? DetailedCreateClaim {
                        vC.Clintname = self.clientName.text!
                        vC.divisionNamestr = self.divisionName.text!
                        vC.engaugement = self.engagementCodeTF.text!
                        vC.datestr = self.claimDate.text!
                        self.navigationController?.pushViewController(vC, animated: true)
                    }
                    
                }
            }) { (fail) in
                print(fail)
                DispatchQueue.main.async {
                   
                }
            }
            
        }

    }
    
}
//DropDown TextFieldDelegateMethosa
extension CreateClaimViewController:UITextFieldDelegate {
    
 
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == clientName {
            //ClientDropDownApi()
            //ClientTable.reloadData()
            getClientListAPI(searchKey: "")
            ClientTable.isHidden = false
            divisionTable.isHidden = true
            engagementTable.isHidden = true
        }else if textField == divisionName {
            divisionTable.isHidden = false
            ClientTable.isHidden = true
            engagementTable.isHidden = true
            getDivisioinListAPI(searchKey: "")
        }else if textField == engagementCodeTF {
            engagementTable.isHidden = false
            divisionTable.isHidden = true
            ClientTable.isHidden = true
        }
    
        return true
    }
    
    
    func  textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 0
        let currentString: NSString = textField.text as! NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if newString.length <= maxLength {
            
            if textField == clientName {
                ClientTable.isHidden = false
            }else if textField == divisionName {
                divisionTable.isHidden = false
            }else if textField  ==  engagementCodeTF{
               engagementTable.isHidden = false
            }
        }
        return true

        
    }
    
   
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    

}


//DropDown tableviewDelegateMethods

extension CreateClaimViewController : UITableViewDataSource, UITableViewDelegate  {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of items in the sample data structure.
        
        var count:Int?
        
        if tableView == self.ClientTable {
            count = isClientFiltered ? filteredClientList.count : clientList.count
        }
        
        if tableView == self.divisionTable {
            count =  isDivisionFiltered ? filteredDivisionList.count :  divisionList.count
        }
        
        if tableView == self.engagementTable {
            count = isEngagementFiltered ? filteredEngagementList.count : engagementList.count

        }
        return count!
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell?
        
        if tableView == self.ClientTable {
            cell = tableView.dequeueReusableCell(withIdentifier: "ClientNameCell", for: indexPath as IndexPath) as! ClientNameCell
            
            self.tableViewHeight.constant = self.ClientTable.contentSize.height
            cell!.textLabel!.text = isClientFiltered ? filteredClientList[indexPath.row].client_name :  clientList[indexPath.row].client_name
        }
        
        if tableView == self.divisionTable {
            cell = tableView.dequeueReusableCell(withIdentifier: "DivisionNameCell", for: indexPath as IndexPath) as! DivisionNameCell
            let previewDetail = isDivisionFiltered ? filteredDivisionList[indexPath.row].division :  divisionList[indexPath.row].division
            self.divisionHeight.constant = self.divisionTable.contentSize.height

            cell!.textLabel!.text = previewDetail
            
        }
        
        if tableView == self.engagementTable {
            cell = tableView.dequeueReusableCell(withIdentifier: "EngageMentCell", for: indexPath as IndexPath) as! EngageMentCell
            let previewDetail = isEngagementFiltered ? filteredEngagementList[indexPath.row].engagement_code :  engagementList[indexPath.row].engagement_code
            self.engageMenttablelHeight.constant = self.engagementTable.contentSize.height
            cell!.textLabel!.text = previewDetail
            
        }
        
        cell?.textLabel?.textColor = #colorLiteral(red: 0.1137254902, green: 0.2794491053, blue: 0.2853239477, alpha: 1)
    
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ClientTable.isHidden = true
        divisionTable.isHidden = true
        engagementTable.isHidden = true
        
        if tableView == self.ClientTable {
            self.clientName.text = isClientFiltered ? filteredClientList[indexPath.row].client_name : clientList[indexPath.row].client_name
            self.Clientid = isClientFiltered ? filteredClientList[indexPath.row].id! : clientList[indexPath.row].id!
            
            getEngagementListAPI(searchKey: Clientid)

        }else if tableView == self.divisionTable{
            self.divisionName.text = isDivisionFiltered ? filteredDivisionList[indexPath.row].division : divisionList[indexPath.row].division
            self.divisionid = isDivisionFiltered ? filteredDivisionList[indexPath.row].id! : divisionList[indexPath.row].id!

        }else if tableView == self.engagementTable {
            self.engagementCodeTF.text = isEngagementFiltered ? filteredEngagementList[indexPath.row].engagement_code : engagementList[indexPath.row].engagement_code
            self.engagugeid = isEngagementFiltered ? filteredEngagementList[indexPath.row].engagement_id! : engagementList[indexPath.row].engagement_id!
    }
 
}

}
extension CreateClaimViewController {
    
    func getClientListAPI(searchKey:String){
        //showLoader()
        let url = BaseUrl.clientDropDown
        NetworkManager.getReponse(url: url, success: { (response) in

            do{
                let clientDetailsResponse = try JSONDecoder().decode(ClientDetails.self, from: response)
                print(clientDetailsResponse)

                if let clientDetails = clientDetailsResponse.Client_detail{
                    //self.clientList.append(contentsOf: clientDetails)
                    self.clientList = clientDetails
                    DispatchQueue.main.async {
                        self.ClientTable.reloadData()
                    }
                }
                
                
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                    alert(alertTitle: "Alert !!!", message: "Getting client details failed", actionTitle: "Ok", viewcontroller: self)
                }
                print(DataNotFound.dataNotFound)
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
        
    }

    
    func getDivisioinListAPI(searchKey:String){
        //showLoader()
        let url = BaseUrl.DivisionDropDown
        NetworkManager.getReponse(url: url, success: { (response) in

            do{
                let divisionDetailsResponse = try JSONDecoder().decode(DivisionDetails.self, from: response)
                if let divisionDetails = divisionDetailsResponse.division_detail{
                    self.divisionList = divisionDetails
                    DispatchQueue.main.async {
                        self.divisionTable.reloadData()
                    }
                }
                
                
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                    alert(alertTitle: "Alert !!!", message: "Getting division details failed", actionTitle: "Ok", viewcontroller: self)
                }
                print(DataNotFound.dataNotFound)
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
        
    }

    
    
    func getEngagementListAPI(searchKey:String){
        //showLoader()
        let url = BaseUrl.engagementDropDown + searchKey
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                let engagementDetailsResponse = try JSONDecoder().decode(EngagementDetails.self, from: response)

                if let engagementDetails = engagementDetailsResponse.engagement_detail{
                    self.engagementList = engagementDetails
                    DispatchQueue.main.async {
                        self.engagementTable.reloadData()
                    }
                }
                
                
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                    alert(alertTitle: "Alert !!!", message: "Getting engament details failed", actionTitle: "Ok", viewcontroller: self)
                }
                print(DataNotFound.dataNotFound)
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
        
    }
    
    
    func GetEngagementPreference(){
        
        GetMethodByusingUrlSession(url: BaseUrl.getEngagementPreference) { (data) in
            print(data)
            
            let status = data["status"] as! String
            if status == "success"{
                var preference = data["preference"] as! [Any]
                
                var engagementprefernce = preference[0] as! [String:Any]
                self.engagementPreference = engagementprefernce["eng_preference"] as! String
                DispatchQueue.main.async {
                if(self.engagementPreference == "0"){
                   self.engagementRoundViewHeight.constant = 0
                    self.engagmentImageView.isHidden = true
                    self.engagementLabel.isHidden = true
                } else {
                    self.engagementRoundViewHeight.constant = 85
                    self.engagmentImageView.isHidden = false
                    self.engagementLabel.isHidden = false
                }
                }
                
            }
            
        }
        
    }
    
    
    func createCliam(param:String, success:@escaping(BaseResponse)->(), failure:@escaping failure){
        DispatchQueue.main.async {
            showLoader()
        }
        var request  = URLRequest(url: URL(string: BaseUrl.addClaim)!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let params = param.data(using:String.Encoding.ascii, allowLossyConversion: false)
        request.httpBody = params
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else{
                DispatchQueue.main.async {
                    hideLoader()
                }
                failure(.dataNotFound)
                return}
            do {
                DispatchQueue.main.async {
                    hideLoader()
                }
                
                let response = try JSONDecoder().decode(BaseResponse.self, from: data)
                if let status = response.status{
                    if(status == "success"){
                        success(response)
                    }else{
                        
                    }
                }
                
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                }
                failure(.dataNotFound)
                print("error")
            }
            
        }
        task.resume()
    }

}

extension CreateClaimViewController{
    func getUnreadMessages_API(completion:@escaping(Int)->()){
        showLoader()
        let url = BaseUrl.getUnreadMessages
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                if let resp =  try JSONSerialization.jsonObject(with: response, options: .mutableLeaves) as? [String:Any]{
                    if let count = resp["msgcount"] as? Int{
                        completion(count)
                    }
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                }
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
        
    }
}
