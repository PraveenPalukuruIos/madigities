//
//  DetailedCreateClaim.swift
//  Digities
//
//  Created by PraveenKumarReddy on 16/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos
import BSImagePicker

class DetailedCreateClaim: UIViewController {
    @IBOutlet weak var welcomeLbl: UILabel!
    
    // Claim Details
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var divisionName: UILabel!
    @IBOutlet weak var engagementCode: UILabel!
    @IBOutlet weak var dateOfClaim: UILabel!
    
    //MARK:- Add Expense Outlets
    // date Errors
    @IBOutlet weak var expenseDateErrorTop: UILabel!
    @IBOutlet weak var errorLine: UIView!
    @IBOutlet weak var dateBottomerrorLbl: UILabel!
    
    
    // Categeory_TF
    @IBOutlet weak var categoryTF: UITextField!
    @IBOutlet weak var subCategoryTF: UITextField!
    
    
    @IBOutlet weak var backHeight: NSLayoutConstraint!
    @IBOutlet weak var expenseAmountTop: NSLayoutConstraint!
    
    
    
    // Views
    @IBOutlet weak var stayView: UIView!
    @IBOutlet weak var numberOfNightsTF: UITextField!
    
    
    //MARK:- FoodView
    @IBOutlet weak var foodView: UIView!
    @IBOutlet weak var CountTF: UITextField!
    var value = 0
    
    @IBOutlet weak var numOfEmploye: UITextField!
    
    
    
    // TravelView
    
    @IBOutlet weak var travelView: UIView!
    @IBOutlet weak var fromTF: UITextField!
    @IBOutlet weak var toTf: UITextField!
    @IBOutlet weak var distanceTF: UITextField!
    @IBOutlet weak var travelEdit: UIButton!
    
    // Employee Outlets
    
    @IBOutlet weak var expenseAmount: UITextField!
    @IBOutlet weak var expenseRemark: UITextField!
    @IBOutlet weak var datePickerTF: UITextField!
    
    
    
    // Reimbursable buttons
    
    @IBOutlet weak var switchOne: UIButton!
    @IBOutlet weak var reimbursableTF: UITextField!
    @IBOutlet weak var rupeesLbl: UILabel!
    @IBOutlet weak var linelbl: UILabel!
    @IBOutlet weak var remiberseErrBottonLbl: UILabel!
    
    @IBOutlet weak var switchTwoTop: NSLayoutConstraint!
    @IBOutlet weak var reimburseTopLbl: UILabel!

    @IBOutlet weak var switch2: UIButton!
    @IBOutlet weak var heightConstant: NSLayoutConstraint!
 
    // ChooseProfiles
    
    
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var ChageBleTopConstant: NSLayoutConstraint!
    
//    var clientList = [ClientDetail]()
    
    var catgeryItems = [String]()
    var catgeryIds = [String]()
    
    var subCatName = [String]()
    var subCatId = [String]()
    
    var catStr = String()
    var subCat = String()
    
    
    
    @IBOutlet weak var catgeoryTable: UITableView!
    @IBOutlet weak var CatHeightConstant: NSLayoutConstraint!
  
    var SubCatData = [String]()
    @IBOutlet weak var subCatGeoryTable: UITableView!
    @IBOutlet weak var subCatHeightConstant: NSLayoutConstraint!
    // Custom Alert
    @IBOutlet weak var ShadowAlertBtn: UIButton!
    @IBOutlet weak var alertView: RoundUIView!
    // ImagePicker
    let imagePicker = UIImagePickerController()
    var SelectedAssets = [PHAsset]()
    var PhotoArray = [UIImage]()
    //    @IBOutlet weak var imageViewPic: UIImageView!
    
    var policydays = Int()
    var Clintname = String()
    var divisionNamestr = String()
    var engaugement = String()
    var datestr = String()
    var client_detail:ClientDetail?
    let date_Picker = UIDatePicker()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.numOfEmploye.textColor = #colorLiteral(red: 0.1481741667, green: 0.3485555053, blue: 0.3549057841, alpha: 1)
        self.switchTwoTop.constant = 77
        
        self.welcomeLbl.text = "Welcome "  + Global.userName
        self.clientName.text = Clintname
        self.divisionName.text = divisionNamestr
        self.engagementCode.text = engaugement
        self.dateOfClaim.text = datestr
        
        self.imagePicker.delegate = self
        backHeight.constant = 185
        expenseAmountTop.constant = 25
        
        stayView.isHidden = true
        travelView.isHidden = true
        foodView.isHidden = true
        ShadowAlertBtn.isHidden = true
        alertView.isHidden = true
        
        categoryTF.delegate = self
        subCategoryTF.delegate = self
        
        self.dateBottomerrorLbl.isHidden = true
        
    }
    
    
    func showDatePicker(){
        //Formate Date
        date_Picker.datePickerMode = .date
        date_Picker.maximumDate = Date()
        date_Picker.minimumDate = Calendar.current.date(byAdding: .day, value: -policydays , to: Date())
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        datePickerTF.inputAccessoryView = toolbar
        datePickerTF.inputView = date_Picker
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        datePickerTF.text = formatter.string(from: date_Picker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getClientListAPI { (clientDetails) in
            
            print(clientDetails)
            
        }
        getUnreadMessages_API { (count) in
            print(count)
        }
        getPolicydays_API{ (count) in
            print(count)
            DispatchQueue.main.async {
                self.policydays = count
                self.showDatePicker()
            }
        }
    }
    
    
    
    // Category Action
    @IBAction func categeory_Action(_ sender: Any) {
        
        categoryTF.becomeFirstResponder()
        
        if categoryTF.text == "" {
            catgeoryTable.isHidden = false
            
        }else{
            catgeoryTable.isHidden = true
        }
        subCatGeoryTable.isHidden = true
        
        
        
        //selectFood
        //107
        //:::: stay
        // 104
        // height backView ::::  260
        
        
        /// chnaged  Travel
        // 350 height
        // 200 --- top expense
        
        // Select Test
        // 25 top
        // back View = 185 height
        
        
    }
    // SubCategory Action
    @IBAction func subCatgeory_Action(_ sender: Any) {
        
        subCategoryTF.becomeFirstResponder()
        
        if subCategoryTF.text == "" {
            subCatGeoryTable.isHidden = false
            
        }else{
            subCatGeoryTable.isHidden = true
        }
        catgeoryTable.isHidden = true
        
        
    }
    
    //MARK:- Select employee_Action
    @IBAction func Select_Employe(_ sender: Any) {
        
        
    }
    
    
    @IBAction func minus_Action(_ sender: Any) {
        value = value - 1
        self.CountTF.text = String(value)
    }
    
    
    @IBAction func plus_Action(_ sender: Any) {
        value = value + 1
        self.CountTF.text = String(value)
    }
    
    
    // OthersInfo_Action
    @IBAction func info_Action(_ sender: Any) {
        
    }
    // Switch1
    @IBAction func Switch_Action(_ sender: Any) {
        
        if switchOne.isSelected == false {
            switchOne.isSelected = true
            
            // 128
            // 63 = top
            
            self.linelbl.isHidden = true
            self.rupeesLbl.isHidden = true
            self.reimbursableTF.isHidden = true
            self.remiberseErrBottonLbl.isHidden = true
            self.reimburseTopLbl.isHidden = true
            self.heightConstant.constant = 100
            self.switchTwoTop.constant = 20
            
        }else{
            switchOne.isSelected = false
            self.linelbl.isHidden = false
            self.rupeesLbl.isHidden = false
            self.reimbursableTF.isHidden = false
            self.remiberseErrBottonLbl.isHidden = false
            self.reimburseTopLbl.isHidden = false
            
            self.heightConstant.constant = 140
            self.switchTwoTop.constant = 77
        }
    }
    
    // Switch2
    
    @IBAction func Switch2_Action(_ sender: Any) {
        
        if switch2.isSelected == true {
            switch2.isSelected = false
            
            
        }else{
            switch2.isSelected = true
        }
        
        
    }
    
    @IBAction func chooseProfile_Action(_ sender: Any) {
        PhotoArray.removeAll()

        ShadowAlertBtn.isHidden = false
        alertView.isHidden = false
        
    }
    
    @IBAction func shadowView_Action(_ sender: Any) {
        ShadowAlertBtn.isHidden = true
        alertView.isHidden = true
        
    }
    
    @IBAction func chooseGallary_Action(_ sender: Any) {
        
        PhotoArray.removeAll()

        self.openGallary()
        
    }
    
    @IBAction func chooseCamera_ACtion(_ sender: Any) {
        
        self.openCamera()
        
    }
    
    
    
    
    @IBAction func multipleImages_Action(_ sender: Any) {
        self.Multiple()
        
    }
    
    
    
    @IBAction func cancel_ACTion(_ sender: Any) {
        ShadowAlertBtn.isHidden = true
        alertView.isHidden = true
    }
    //MARK:-Submit_Action
    @IBAction func Submit_Action(_ sender: Any) {
        
        if datePickerTF.text!.isEmpty{
            self.errorLine.backgroundColor = .red
            self.dateBottomerrorLbl.isHidden = false
            self.expenseDateErrorTop.textColor = .red
            self.datePickerTF.placeholder = ""
            return
        }
        
        guard let expCategory = self.categoryTF.text,expCategory != "" else {
            return
        }
        
        guard let expSubCategory = self.subCategoryTF.text,expSubCategory != "" else {
            return
        }
   
        if self.categoryTF.text == "food"{
            
            guard let NumberOfEmployee = self.numOfEmploye.text,NumberOfEmployee != "" else {
                return
            }
        }else if self.categoryTF.text == "stay" {
            
            guard let numberOfNights = self.numberOfNightsTF.text,numberOfNights != "" else {
                return
            }
            
        }else if self.categoryTF.text == "travel"{
            
            guard let fromLoction = self.fromTF.text,fromLoction != "" else {
                return
            }
            guard let toLoction = self.toTf.text,toLoction != "" else {
                return
            }
            guard let distance = self.distanceTF.text,distance != "" else {
                return
            }
            
        }
        
        guard let expanseAmount = self.expenseAmount.text,expanseAmount != "" else {
            return
        }
        
        var params = [
            "user_id":Global.ID,
            "name":Global.userName,
            "comp_id":Global.compID,
            "client_id":"76",
            "claim_date":self.dateOfClaim.text,
            "client_name":self.clientName.text,
            "claim_id":"1901",
            "exp_id":"2016",
            "type":"add",
            "photoid":"0",
            "files":""
        ]
        
        POSTMethodByusingUrlSession(url: "http://projectdemo.co.in/expensetracking/Androidapi/upload_bill", Prameters: params) { (data) in
            print(data)
        }
        
        
    }
    //MARK:-addAnother_EXpenseAction
    @IBAction func addAnother_EXpenseAction(_ sender: Any) {
        if datePickerTF.text!.isEmpty{
            self.errorLine.backgroundColor = .red
            self.dateBottomerrorLbl.isHidden = false
            self.expenseDateErrorTop.textColor = .red
            self.datePickerTF.placeholder = ""
            return
        }
    }
    
    
    @IBAction func expense_AmountAction(_ sender: Any) {
        
        var params = ["emp_id":Global.empID,
                      "comp_id":Global.compID,
                      "no_of_nights":self.numberOfNightsTF.text,
                      "catesel":self.catStr,
                      "empsel":"",
                      "noofemp_tg":self.numOfEmploye.text,
                      "distance":"",
                      "scatesel":self.subCat,
                      "catename":self.categoryTF.text,
                      "otherscount":self.CountTF.text
            ] as [String : Any]
        
        print(params)
        
        POSTMethodByusingUrlSession(url: "http://projectdemo.co.in/expensetracking/Androidapi/get_expense_amount", Prameters: params) { (data) in
            print(data)
            
            if data != nil {
                
                var amount = data["amount"] as! String
                
                
                DispatchQueue.main.async {
                    if amount == self.expenseAmount.text! {
                        self.reimburseTopLbl.text = ""
                        self.linelbl.backgroundColor = #colorLiteral(red: 0.1481741667, green: 0.3485555053, blue: 0.3549057841, alpha: 1)
                        self.remiberseErrBottonLbl.text = ""
                        self.reimbursableTF.text = self.expenseAmount.text!
                        
                        
                    }else{
                        self.reimbursableTF.text = self.expenseAmount.text!
                        
                        self.reimburseTopLbl.text = "Reimbursable Amount*"
                        self.linelbl.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
                        self.remiberseErrBottonLbl.text = "Not in accorrdance with policy - Specifield Amount is €\(amount) "
                    }
                }
                
            }
        }
        
        
    }
    
}

extension DetailedCreateClaim:UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == categoryTF {
            
            GetMethodByusingUrlSession(url: "https://projectdemo.co.in/expensetracking/Androidapi/get_expense_category_list/\(Global.compID)/") { (data) in
                print(data)
                var status = data["status"] as! String
                
                if status == "success" {
                    var array = data["exp_cat_detail"] as! [Any]
                    self.catgeryIds.removeAll()
                    self.catgeryItems.removeAll()
                    for i in 0..<array.count {
                        print(i)
                        var dict = array[i] as! [String:Any]
                        var category_id = dict["category_id"] as! String
                        var exp_category_name = dict["exp_category_name"] as! String
                        self.catgeryIds.append(category_id)
                        self.catgeryItems.append(exp_category_name)
                        print(category_id)
                        
                        DispatchQueue.main.async {
                            self.catgeoryTable.reloadData()
                            
                        }
                        
                    }
                    
                }
            }
            catgeoryTable.isHidden = false
            subCatGeoryTable.isHidden = true
            self.catgeoryTable.reloadData()
            
        }else if textField == subCategoryTF {
            subCatGeoryTable.isHidden = false
            catgeoryTable.isHidden = true
            self.subCatGeoryTable.reloadData()
            
        }
        
        if textField == datePickerTF{
            self.errorLine.backgroundColor = .darkGray
            self.dateBottomerrorLbl.isHidden = true
            self.expenseDateErrorTop.textColor = .black
        }
        
        return true
    }


    func  textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {


        let maxLength = 0
        let currentString: NSString = textField.text as! NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString

        if newString.length <= maxLength {

            if textField == categoryTF {
                catgeoryTable.isHidden = false
                subCatGeoryTable.isHidden = true
            }else if textField == subCategoryTF {
                subCatGeoryTable.isHidden = false
                catgeoryTable.isHidden = true            }

        }
        return true


    }

}



extension DetailedCreateClaim : UITableViewDataSource, UITableViewDelegate  {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of items in the sample data structure.
        
        var count:Int?
        
        if tableView == self.catgeoryTable {
            count = catgeryItems.count
        }
        
        if tableView == self.subCatGeoryTable {
            count = SubCatData.count
        }
        
        return count!
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell?
        
        if tableView == self.catgeoryTable {
            cell = tableView.dequeueReusableCell(withIdentifier: "CatgeoryCell", for: indexPath as IndexPath) as! CatgeoryCell
            cell!.textLabel!.text = catgeryItems[indexPath.row]
            cell?.textLabel?.textColor = #colorLiteral(red: 0.1138102636, green: 0.2794491053, blue: 0.2853239477, alpha: 1)
            
        }
        
        if tableView == self.subCatGeoryTable {
            cell = tableView.dequeueReusableCell(withIdentifier: "SubCatGeoryCell", for: indexPath as IndexPath) as! SubCatGeoryCell
            let previewDetail = SubCatData[indexPath.row]
            cell!.textLabel!.text = previewDetail
            cell?.textLabel?.textColor = #colorLiteral(red: 0.1138102636, green: 0.2794491053, blue: 0.2853239477, alpha: 1)

            
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        catgeoryTable.isHidden = true
        subCatGeoryTable.isHidden = true
        self.catStr = catgeryIds[indexPath.row]
        
        
        if tableView == self.catgeoryTable {
            
            //selectFood
            //107
            //:::: stay
            // 104
            // height backView ::::  260
            
            
            /// chnaged  Travel
            // 350 height
            // 200 --- top expense
            
            // Select Test
            // 25 top
            // back View = 185 height
            
            self.categoryTF.text = catgeryItems[indexPath.row]
            if self.categoryTF.text == "food"{
              backHeight.constant = 260
                expenseAmountTop.constant = 107
                
                stayView.isHidden = true
                travelView.isHidden = true
                foodView.isHidden = false
                
                
                var str =   catgeryIds[indexPath.row]  as! String
                GetMethodByusingUrlSession(url: "https://projectdemo.co.in/expensetracking/Androidapi/get_expense_sub_category_list/\(Global.compID)/\(str)") { (data) in
                    print(data)
                    
                    if  let status = data["status"] as? String {
                        
                        self.SubCatData.removeAll()
                        self.subCatId.removeAll()
                        
                        if status == "success"{
                            var subcatData = data["exp_sub_cat_detail"] as! [Any]
                            
                            for i in 0..<subcatData.count {
                                
                                var dict = subcatData[i] as!
                                    [String:Any]
                                
                                var subcatName = dict["exp_sub_category_name"] as! String
                                
                                var sub_category_id = dict["sub_category_id"] as! String
                                
                                self.SubCatData.append(subcatName)
                                self.subCatId.append(sub_category_id)
                                
                                DispatchQueue.main.async {
                                    self.subCatGeoryTable.reloadData()
                                }
                                
                            }
                        }
                        
                    }
                    
                    self.subCat = self.subCatId[indexPath.row]

                }
        
            }else if self.categoryTF.text == "stay"{
                
             var str =   catgeryIds[indexPath.row]  as! String
                GetMethodByusingUrlSession(url: "https://projectdemo.co.in/expensetracking/Androidapi/get_expense_sub_category_list/\(Global.compID)/\(str)") { (data) in
                    print(data)
                    
                    if  let status = data["status"] as? String {
                        
                       self.SubCatData.removeAll()
                       self.subCatId.removeAll()
                  
                        if status == "success"{
                            var subcatData = data["exp_sub_cat_detail"] as! [Any]
                            
                            for i in 0..<subcatData.count {
                                
                                var dict = subcatData[i] as!
                                [String:Any]
                                
                                var subcatName = dict["exp_sub_category_name"] as! String
                                
                                var sub_category_id = dict["sub_category_id"] as! String
                                
                                self.SubCatData.append(subcatName)
                                self.subCatId.append(sub_category_id)
                                
                                DispatchQueue.main.async {
                                    self.subCatGeoryTable.reloadData()
                                }
                             
                            }
                        }
                        self.subCat = self.subCatId[indexPath.row]

                    }
                   
                    
                }
                
                
                backHeight.constant = 185
                expenseAmountTop.constant = 25
                stayView.isHidden = false
                travelView.isHidden = true
                foodView.isHidden = true
                subCatHeightConstant.constant = 90
                

                
            }else if self.categoryTF.text == "travel"{
                stayView.isHidden = true
                travelView.isHidden = false
                foodView.isHidden = true
                
                backHeight.constant = 350
                expenseAmountTop.constant = 200
                subCatHeightConstant.constant = 90
                
                var str =   catgeryIds[indexPath.row]  as! String
                GetMethodByusingUrlSession(url: "https://projectdemo.co.in/expensetracking/Androidapi/get_expense_sub_category_list/\(Global.compID)/\(str)") { (data) in
                    print(data)
                    
                    if  let status = data["status"] as? String {
                        
                        self.SubCatData.removeAll()
                        self.subCatId.removeAll()
                        
                        if status == "success"{
                            var subcatData = data["exp_sub_cat_detail"] as! [Any]
                            
                            for i in 0..<subcatData.count {
                                
                                var dict = subcatData[i] as!
                                    [String:Any]
                                
                                var subcatName = dict["exp_sub_category_name"] as! String
                                
                                var sub_category_id = dict["sub_category_id"] as! String
                                
                                self.SubCatData.append(subcatName)
                                self.subCatId.append(sub_category_id)
                                
                                DispatchQueue.main.async {
                                    self.subCatGeoryTable.reloadData()
                                }
                                
                            }
                        }
                        
                    }
                    self.subCat = self.subCatId[indexPath.row]

                    
                }
          
            }

            
        }else{
            self.subCategoryTF.text = SubCatData[indexPath.row]
            
            self.subCat  = subCatId[indexPath.row]
            
        }
        
    }
    
    
    
}
extension DetailedCreateClaim:UIImagePickerControllerDelegate,UINavigationControllerDelegate

{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        

        
        guard let pickedImage = info[.originalImage] as? UIImage else {
            return
        }
        PhotoArray.append(pickedImage)

        
        self.imageView1.image = PhotoArray[0]
        self.imageView1.image = PhotoArray[1]

        ShadowAlertBtn.isHidden = true
        alertView.isHidden = true
        
        
//         imageViewPic.contentMode = .scaleToFill
        picker.dismiss(animated: true, completion: nil)
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
 
    
    func Multiple(){
        
        // create an instance
        let vc = BSImagePickerViewController()
        
        
        vc.maxNumberOfSelections = 2
        var value = 0
        
        //display picture gallery
        self.bs_presentImagePickerController(vc, animated: true,
                                             select: { (asset: PHAsset) -> Void in
                                                
                                                value = value + 1
                                                print(value)
                                                DispatchQueue.main.async {
                                                    
                                                    if  value == 2 {
                                                        let alert = UIAlertController(title: "Alert!", message: "Maximum limit is 2", preferredStyle: UIAlertController.Style.alert)
                                                        
                                                        // add an action (button)
                                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                                        
                                                        // show the alert
                                                        self.present(alert, animated: true, completion: nil)
                                                    }
                                                    
                                                    
                                                    
                                                }
                                                
                                                
                                                
        }, deselect: { (asset: PHAsset) -> Void in
            // User deselected an assets.
            
            
            
        }, cancel: { (assets: [PHAsset]) -> Void in
            // User cancelled. And this where the assets currently selected.
            
            
            
        }, finish: { (assets: [PHAsset]) -> Void in
            // User finished with these assets
         
            for i in 0..<assets.count
            {
                self.SelectedAssets.append(assets[i])
            }
            
            self.convertAssetToImages()
            
        }, completion: nil)
        
        
    }
    
    
    func convertAssetToImages() -> Void {
        
        if SelectedAssets.count != 0{
            
            
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            var thumbnail = UIImage()
            option.isSynchronous = true
            
            
            manager.requestImage(for: SelectedAssets[0], targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                thumbnail = result!
                
            })
            
            
            
            
            let data = thumbnail.jpegData(compressionQuality: 0.7)
            
            let newImage = UIImage(data: data!)
            
            
            
            manager.requestImage(for: SelectedAssets[1], targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                thumbnail = result!
                
            })
            let data2 = thumbnail.jpegData(compressionQuality: 0.7)
            
            let newImage1 = UIImage(data: data2!)
            PhotoArray.append(newImage!)
            PhotoArray.append(newImage1!)
            self.imageView1.image = PhotoArray[0]
            self.imageView2.image = PhotoArray[1]

            
           
            
            
            ShadowAlertBtn.isHidden = true
            alertView.isHidden = true
           
            
        }
        
       
        
    }
    
    
    func openCamera()
    {
         PhotoArray.removeAll()

        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    func openGallary()
    {
        
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension DetailedCreateClaim{
func getClientListAPI(completion:@escaping(ClientDetail)->()){
    //showLoader()
    let url = BaseUrl.getclaimbyid
    NetworkManager.getReponse(url: url, success: { (response) in
        do{
            let clientDetailsResponse = try JSONDecoder().decode(ClientDetails.self, from: response)
            print(clientDetailsResponse)
            
            if let clientDetails = clientDetailsResponse.Client_detail{
                if clientDetails.count > 0{
                    completion(clientDetails[0])
                }
            }
        }catch{
            DispatchQueue.main.async {
                hideLoader()
                alert(alertTitle: "Alert !!!", message: "Getting client details failed", actionTitle: "Ok", viewcontroller: self)
            }
            print(DataNotFound.dataNotFound)
        }
    }) { (err) in
        hideLoader()
        print(err)
    }
    
}
}

extension DetailedCreateClaim{
    func getUnreadMessages_API(completion:@escaping(Int)->()){
        showLoader()
        let url = BaseUrl.getUnreadMessages
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                if let resp =  try JSONSerialization.jsonObject(with: response, options: .mutableLeaves) as? [String:Any]{
                    if let count = resp["msgcount"] as? Int{
                        completion(count)
                    }
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                }
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
        
    }
    
    func getPolicydays_API(completion:@escaping(Int)->()){
        showLoader()
        let url = BaseUrl.getPolicyDays
//        {
//            "status": "success",
//            "policydays": [{
//            "policy_days": "20"
//            }]
//        }
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                if let resp =  try JSONSerialization.jsonObject(with: response, options: .mutableLeaves) as? [String:Any]{
                    if let policyDaysArray = resp["policydays"] as? [[String:Any]]{
                        if let policy_days = policyDaysArray[0]["policy_days"] as? String{
                            if let days = Int(policy_days){
                                completion(days)
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                }
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
        
    }
}
