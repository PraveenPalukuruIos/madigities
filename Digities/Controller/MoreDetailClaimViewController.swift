//
//  MoreDetailClaimViewController.swift
//  Digities
//
//  Created by vamsi on 11/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class MoreDetailClaimViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var welcomLbl: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var navTitle: UILabel!
    
    var detailClaimData:DetailClaimsData?
    var isFrom:TitleType?
    
    var expenseDetails:ExpenseData?
    var claimBillDetails:ClaimBillData?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        getDataFromServer()
        setupUI()
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.endEditing(true)
        ImgVariables.removeAllViews()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    func setupUI(){
        welcomLbl.text = "Welcome \(Global.userName)"
        tableView.estimatedRowHeight = 230
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = .init(frame: .zero)
    }
    
    
    func getDataFromServer(){
        getExpenseDetails_API { (expData) in
            self.expenseDetails = expData
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
        getClaimBillDetails_API { (claimData) in
            self.claimBillDetails = claimData
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }

    }

    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
extension MoreDetailClaimViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MoreDetailTableViewCell.identifier, for: indexPath) as! MoreDetailTableViewCell
        if let expDetails = expenseDetails{
        cell.configureCell(expDetails: expDetails, title: isFrom)
        }
        if let claimDetails = claimBillDetails{
            cell.setupClaimBill(claimDetails: claimDetails)
        }
        
        cell.vc = self
        
        return cell
    }
    
}


extension MoreDetailClaimViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 650
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
extension MoreDetailClaimViewController{
    
    func getExpenseDetails_API(completion:@escaping(ExpenseData)->()){
        showLoader()
        var tranId = ""
        if let data = detailClaimData{
            if let id = data.trans_id{
                tranId = id
            }
        }
        let url = BaseUrl.getExpenseById + tranId
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                let exp = try JSONDecoder().decode(ExpenseDetailsHead.self, from: response)
                if let expenseData = exp.expensedata{
                    completion(expenseData)
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                }
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
}
}

extension MoreDetailClaimViewController{
    
    func getClaimBillDetails_API(completion:@escaping(ClaimBillData)->()){
        showLoader()
        var tranId = ""
        if let data = detailClaimData{
            if let id = data.trans_id{
                tranId = id
            }
        }
        let url = BaseUrl.getClaimBillByTranId + tranId
        NetworkManager.getReponse(url: url, success: { (response) in
            do{
                let resp = try JSONDecoder().decode(ClaimBill.self, from: response)
                if let claimBill = resp.expenseimage{
                    //completion(claimBill)
                }
                DispatchQueue.main.async {
                    hideLoader()
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                }
            }
        }) { (err) in
            hideLoader()
            print(err)
        }
    }
}
