

import UIKit
import Alamofire

class SignInViewController: UIViewController {
    
    @IBOutlet weak var bizerView: UIView!
    //MARK:-WElcom digities backgroungView
    @IBOutlet weak var signInView: UIView!
    @IBOutlet weak var signViewHeghtConstant: NSLayoutConstraint!
    
    //MARK:-USENAME OUTLETS
    
    @IBOutlet weak var userNameViewTop: NSLayoutConstraint!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var placeHolderUserNameLbl: UILabel!
    @IBOutlet weak var errorBorderLbl: UILabel!
    @IBOutlet weak var enterUserNAmeErrLbl: UILabel!
    @IBOutlet weak var UserlineHeightLbl: NSLayoutConstraint!
    
    
    //MARK:-PASSWORD OUTLETS
    @IBOutlet weak var passwordBackView: UIView!
    @IBOutlet weak var placeHollderPasswordLbl: UILabel!
    
    @IBOutlet weak var passwordTf: UITextField!
    @IBOutlet weak var passwordLineLbl: UILabel!
    @IBOutlet weak var enterPasswordErLbl: UILabel!
    @IBOutlet weak var passwordHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var showhideImageView: UIImageView!
    
    @IBOutlet weak var passwordShowHIdeBtn: UIButton!
    
    @IBOutlet weak var paswdLineHeightLbl: NSLayoutConstraint!
    
    
    //MARK:- SignButton
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var BtntopConstant: NSLayoutConstraint!
    
    
    @IBOutlet weak var ClearBtn: UIButton!
    
    var boolCheck = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        userNameTF.delegate = self
        passwordTf.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        setUpUI()
        hidePasswordView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.resignFirstResponder()
        view.endEditing(true)
    }
    
    
    //MARK:- ERROR USERNAME-
    func ErrorUserName(){
        UIView.animate(withDuration: 3.0, delay: 0.0, options: .curveEaseIn, animations: {
            self.placeHolderUserNameLbl.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            self.errorBorderLbl.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            self.enterUserNAmeErrLbl.isHidden = false
            self.enterUserNAmeErrLbl.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        }, completion: nil)
        
    }
    
    func setupCorrectUserNameUI(){
        UIView.animate(withDuration:3.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.placeHolderUserNameLbl.textColor = #colorLiteral(red: 0.1137254902, green: 0.2794491053, blue: 0.2853239477, alpha: 1)
            self.errorBorderLbl.backgroundColor = #colorLiteral(red: 0.1137254902, green: 0.2794491053, blue: 0.2853239477, alpha: 1)
            self.enterUserNAmeErrLbl.isHidden = true
            self.enterUserNAmeErrLbl.textColor = #colorLiteral(red: 0.1137254902, green: 0.2794491053, blue: 0.2853239477, alpha: 1)
        }, completion: nil)
    }
    
    func hidePasswordView(){
        UIView.animate(withDuration:3.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.passwordBackView.isHidden = true
            self.passwordHeightConstant.constant = 0
            self.passwordBackView.layoutIfNeeded()
        }, completion: nil)
    }
    
    func showPasswordView(){
        UIView.animate(withDuration:3.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.passwordBackView.isHidden = false
            self.passwordHeightConstant.constant = 85
            self.passwordBackView.layoutIfNeeded()
        }, completion: nil)
    }
    
    //MARK:- ERROR PASSWORD-
    
    func ErrorPassword(){
        UIView.animate(withDuration: 3.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.enterPasswordErLbl.text = "Wrong Password"
            self.placeHollderPasswordLbl.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            self.passwordLineLbl.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            self.enterPasswordErLbl.isHidden = false
            self.enterPasswordErLbl.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            
        }, completion: nil)
    }
    
    func setupCorrectPasswordUI(){
        UIView.animate(withDuration:3.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.placeHollderPasswordLbl.textColor = #colorLiteral(red: 0.1137254902, green: 0.2794491053, blue: 0.2853239477, alpha: 1)
            self.passwordLineLbl.backgroundColor = #colorLiteral(red: 0.1137254902, green: 0.2794491053, blue: 0.2853239477, alpha: 1)
            self.enterPasswordErLbl.isHidden = true
            self.enterPasswordErLbl.textColor = #colorLiteral(red: 0.1137254902, green: 0.2794491053, blue: 0.2853239477, alpha: 1)
        }, completion: nil)
    }
    override func viewDidLayoutSubviews() {
        self.bizerView.roundCorners(cornerRadius: 25)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setUpUI(){
        self.userNameTF.tintColor = #colorLiteral(red: 0.1400840878, green: 0.261497587, blue: 0.2704961896, alpha: 1)
        self.passwordTf.tintColor = #colorLiteral(red: 0.1400840878, green: 0.261497587, blue: 0.2704961896, alpha: 1)
        self.signInView.layer.cornerRadius = 20
        self.signInView.clipsToBounds = true
        self.signViewHeghtConstant.constant = 400
        self.hidePasswordView()
        self.setupCorrectUserNameUI()
        userNameViewTop.constant = 50
        placeHolderUserNameLbl.textColor = #colorLiteral(red: 0.1400840878, green: 0.261497587, blue: 0.2704961896, alpha: 1)
        enterUserNAmeErrLbl.isHidden = true
        userNameTF.becomeFirstResponder()
        BtntopConstant.constant = 20
        ClearBtn.isHidden = true
    }
    
    @IBAction func SignIn_Action(_ sender: Any) {
        
        guard let userName = self.userNameTF.text , userName != ""else {
            enterUserNAmeErrLbl.text = "Enter Username"
            ErrorUserName()
            return
        }
        
        if boolCheck{
            guard let password = self.passwordTf.text else{return}
            if password == ""{
                self.ErrorPassword()
                self.enterPasswordErLbl.text = "Enter Password"
            }

        }
        
        
        checkUserName(userName: userName) {
            DispatchQueue.main.async {
                guard let password = self.passwordTf.text else{return}
                if password != ""{
                    self.hitSignInAPI(userName: userName, password: password)
                }
            }
        }
    }
    
    
    func checkUserName(userName:String,completion:@escaping()->()){
        checkUserName_API(userName: userName, success: { (resp) in
            print(resp)
            guard let status = resp["status"] as? String else{return}
            if status == "Success"{
                DispatchQueue.main.async {
                    self.boolCheck = true
                    self.passwordTf.becomeFirstResponder()
                    self.showPasswordView()
                    self.setupCorrectUserNameUI()
                    self.signViewHeghtConstant.constant = 450
                    self.UserlineHeightLbl.constant = 1.5
                    self.errorBorderLbl.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                    self.userNameViewTop.constant = 25
                    self.placeHolderUserNameLbl.textColor = #colorLiteral(red: 0.1400840878, green: 0.261497587, blue: 0.2704961896, alpha: 1)
                    self.BtntopConstant.constant = 135
                    self.ClearBtn.isHidden = false
                }
                completion()
            }else{
                DispatchQueue.main.async {
                    self.ErrorUserName()
                    self.enterUserNAmeErrLbl.text = "No user found"

                }
            }
        }) { (fail) in
            DispatchQueue.main.async {
                self.enterUserNAmeErrLbl.text = "No user found"

                self.ErrorUserName()
            }
        }

    }
    
    
    func hitSignInAPI(userName:String,password:String){
        self.signIn_API(userName: userName, password: password, success: { (user) in
            print(user)
            print(user.email as Any)
            defaults.setValue(user.username ?? "", forKey: "name")
            defaults.setValue(user.comp_id ?? "", forKey: "compId")
            defaults.setValue(user.emp_id ?? "", forKey: "empId")
            defaults.setValue(user.id ?? "", forKey: "ID")
            DispatchQueue.main.async {
                let swVC:SWRevealViewController =  UIStoryboard(storyboard: .Home).controller()
                self.navigationController?.pushViewController(swVC, animated: true)
            }
        }) { (fail) in
            print(fail)
            DispatchQueue.main.async {
                self.ErrorPassword()
            }
        }
    }
    @IBAction func userNameTFAction(_ sender: UITextField) {
        if sender.text == ""{
            self.setupCorrectUserNameUI()
        }
    }
    @IBAction func passwordTFaction(_ sender: UITextField) {
        if sender.text == ""{
            self.setupCorrectPasswordUI()
        }
    }
    
    @IBAction func PasswordShowHide_Action(_ sender: Any) {
        if passwordShowHIdeBtn.isSelected == true {
            passwordShowHIdeBtn.isSelected = false
            showhideImageView.image = #imageLiteral(resourceName: "invisible")
            passwordTf.isSecureTextEntry = true
        }else{
            passwordShowHIdeBtn.isSelected = true
            showhideImageView.image = #imageLiteral(resourceName: "Visible")
            passwordTf.isSecureTextEntry = false
        }
    }
    
    @IBAction func ClearAction(_ sender: Any) {
        self.userNameTF.text = ""
        self.passwordTf.text = ""
        UIView.animate(withDuration: 4.0, delay: 0.0, options: .curveEaseIn, animations: {
            self.setUpUI()
        }, completion: nil)
    }
    
}

extension UIView {
    func roundCorners(cornerRadius: Double) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
}

extension SignInViewController{
    func signIn_API(userName:String,password:String,success:@escaping(User)->(),failure:@escaping failure){
        DispatchQueue.main.async {
            showLoader()
        }
        var request  = URLRequest(url: URL(string: BaseUrl.login)!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let params = "usermail=\(userName)&password=\(password)".data(using:String.Encoding.ascii, allowLossyConversion: false)
        request.httpBody = params
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else{
                DispatchQueue.main.async {
                    hideLoader()
                }
                failure(.dataNotFound)
                return}
            do {
                DispatchQueue.main.async {
                    hideLoader()
                }
                let userData = try JSONDecoder().decode(UserData.self, from: data)
                if let user = userData.user{
                    print(user)
                    success(user)
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                }
                failure(.dataNotFound)
                print("error")
            }
            
        }
        task.resume()
    }
}

extension SignInViewController{
    func checkUserName_API(userName:String,success:@escaping([String:Any])->(),failure:@escaping failure){
        showLoader()
        var request  = URLRequest(url: URL(string: BaseUrl.checkuser)!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let params = "usermail=\(userName)".data(using:String.Encoding.ascii, allowLossyConversion: false)
        request.httpBody = params
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                hideLoader()
            }
            guard let data = data else{
                DispatchQueue.main.async {
                    hideLoader()
                }
                failure(.dataNotFound)
                return}
            do {
                DispatchQueue.main.async {
                    hideLoader()
                }
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let response = json as? [String:Any]{
                    success(response)
                    
                    
                }
            }catch{
                DispatchQueue.main.async {
                    hideLoader()
                }
                failure(.dataNotFound)
                print("error")
            }
            
        }
        task.resume()
    }
}

extension SignInViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 88{
            self.setupCorrectUserNameUI()
        }else if textField.tag == 99{
            self.setupCorrectPasswordUI()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text == ""{
        if textField.tag == 88{
            self.setupCorrectUserNameUI()
        }else if textField.tag == 99{
            self.setupCorrectPasswordUI()
        }
    }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
}
