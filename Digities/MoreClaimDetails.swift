//
//  MoreClaimDetails.swift
//  Digities
//
//  Created by vamsi on 12/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation

struct ExpenseDetailsHead:Codable {
    var status:String?
    var expensedata:ExpenseData?
}

struct ExpenseData:Codable {
    var trans_id:String?
    var master_id:String?
    var exp_cat_id:String?
    var exp_cat_name:String?
    var exp_sub_cat_id:String?
    var exp_sub_cat_name:String?
    var employee_id:String?
    var expense_date:String?
    var exp_amount:String?
    var created_on:String?
    var reimbursable:String?
    var reimbursable_amount:String?
    var chargable_to_client:String?
    var expense_remarks:String?
    var additional1:String?
    var additional2:String?
    var date_of_payment:String?
    var paid_amt:String?
    var rejected_amt:String?
    var appr_remark:String?
    var approved_amt:String?
    var claim_status:String?
    var status:String?
    var general:String?
    var no_of_employee:String?
    var no_of_nights : String?
    var distance:String?
    var to_loc:String?
    var from_loc:String?
    var others:String?
    var limit_amount:String?
}

struct ClaimBill:Codable {
    var expenseimage : [ClaimBillData]?
}

struct ClaimBillData:Codable {
    var id : String?
    var comp_i : String?
    var client_i : String?
    var trans_id: String?
    var filename: String?
    var emp_id : String?
    var original: String?
    var uplo : String?
    var upload_at: String?
    var upload_by: String?
    var status : String?
}
