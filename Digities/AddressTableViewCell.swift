//
//  AddressTableViewCell.swift
//  Digities
//
//  Created by vamsi on 05/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var designationLbl: UILabel!
    @IBOutlet weak var phoneNumberlbl: UILabel!

    var callBack:((_ phoneNumber:String)->())?
    var vc:UIViewController?
    
    var phoneNumber = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func configureCell(with employee:EmployeeData){
        profilePic.isUserInteractionEnabled = true
        profilePic.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(zoomImage)))
//        profilePic.image = #imageLiteral(resourceName: "Engaement code")
        nameLbl.text = employee.name ?? ""
        designationLbl.text = employee.designation ?? ""
        let phone_Number = employee.phone_number ?? ""
        phoneNumberlbl.text = phone_Number
        phoneNumber = phone_Number
        
    }
    
    @objc func zoomImage(){
        if let vc = vc{
        profilePic.zoomAnimation(inViewController: vc)
        }
    }
    
    
    @IBAction func phoneCallBtnTapped(_ sender: UIButton) {
        if !phoneNumber.isEmpty{
            callBack?(phoneNumber)
        }
    }
    
    
}
