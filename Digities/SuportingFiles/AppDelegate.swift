//
//  AppDelegate.swift
//  Digities
//
//  Created by vamsi on 03/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UINavigationBar.appearance().backgroundColor = appGreenBackground
        if let isLogin = defaults.value(forKey: "name") as? String{
            if !isLogin.isEmpty{
                let swVC:SWRevealViewController =  UIStoryboard(storyboard: .Home).controller()
                setRootViewController(viewController: swVC)
            }
        }
        return true
    }

}
