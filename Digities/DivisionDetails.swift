//
//  DivisionDetails.swift
//  Digities
//
//  Created by Santhana on 28/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation

struct DivisionDetails:Codable {
    var status:String?
    var division_detail:[DivisionDetail]?
}

struct DivisionDetail:Codable {
    var id:String?
    var division:String?
}
