//
//  ClientDetails.swift
//  Digities
//
//  Created by Santhana on 28/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation

struct ClientDetails:Codable {
    var status:String?
    var Client_detail:[ClientDetail]?
}

struct ClientDetail:Codable {
    var id:String?
    var comp_claim_id:String?
    var client_name:String?
    var comp_id:String?
    var client_id:String?
    var division_id:String?
    var division_name:String?
    var employee_id:String?
    var claim_date:String?
    var engagement_code:String?
    var total_expense_amt:String?
    var total_reimburse_amt:String?
    var total_rejected_amt:String?
    var total_approved_amt:String?
    var total_paid_amt:String?
    var approved_by:String?
    var created_by:String?
    var created_on:String?
}

