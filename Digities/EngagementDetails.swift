//
//  EngagementDetails.swift
//  Digities
//
//  Created by Santhana on 28/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation

struct EngagementDetails:Codable {
    var status:String?
    var engagement_detail:[EngagementDetail]?
}

struct EngagementDetail:Codable {
    var engagement_id:String?
    var engagement_code:String?
}
