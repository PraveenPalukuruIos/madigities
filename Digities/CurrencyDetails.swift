//
//  CurrencyDetails.swift
//  Digities
//
//  Created by vamsi on 11/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation


struct CurrencyHead:Codable {
    var status:String?
    var currency:CurrencyData?
}

struct CurrencyData:Codable {
    var id:String?
    var symbol:String?
    var currency:String?
    var status:String?
}
