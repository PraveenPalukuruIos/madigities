//
//  User.swift
//  GreenApple
//
//  Created by vamsi on 12/05/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation

struct UserData:Codable{
    var status:String?
    var user:User?
}

struct User :Codable{
    var id:String?
    var comp_id:String?
    var emp_id:String?
    var email:String?
    var password:String?
    var username:String?
    var contact_no:String?
    var address:String?
    var city:String?
    var state:String?
    var country:String?
    var picture:String?
    var role:String?
    var random_code:String?
    var last_pass_change:String?
    var created_on:String?
    var status:String?
    var additional1:String?
    var additional2:String?

}

