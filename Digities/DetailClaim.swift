//
//  DetailClaim.swift
//  Digities
//
//  Created by vamsi on 08/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation

struct DetailClaims:Codable {
    var status:String?
    var allexpense:[DetailClaimsData]?
}

struct DetailClaimsData:Codable {
    var trans_id:String?
    var exp_cat_name:String?
    var exp_amount:String?
    var expense_date:String?
}
