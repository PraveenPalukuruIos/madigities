//
//  AllMessages.swift
//  Digities
//
//  Created by vamsi on 14/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation
//{
//    "status": "success",
//    "messages": [{
//    "id": "179",
//    "created_date": "2019-05-05 01:02:11",
//    "sender_id": "98",
//    "receiver_id": "330",
//    "subject": "first message to all",
//    "message": "welcome to sakthi motors",
//    "status": "0"
//    }]
//}

struct AllNotificationsHead:Codable{
    var status:String?
    var messages:[AllNotificationsData]?
}

struct AllNotificationsData :Codable{
    var id:String?
    var created_date:String?
    var sender_id:String?
    var receiver_id:String?
    var subject:String?
    var message:String?
    var status:String?
}
