//
//  Employee.swift
//  Digities
//
//  Created by vamsi on 05/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation
//{
//    "status": "success",
//    "employee": [
//    {
//    "id": "81",
//    "comp_id": "1",
//    "name": "ADa",
//    "company_name": "Gofrugal Pvt. LTd.,updated",
//    "phone_number": "89148516541",
//    "email": "",
//    "date_of_joining": "2019-04-19",
//    "emp_id": "86",
//    "designation": "Team Leader",
//    "division": "34",
//    "approver1": "31",
//    "approver2": "",
//    "date_of_birth": "0000-00-00",
//    "gender": "1",
//    "location": "",
//    "permanent_address": "",
//    "alt_phone_no": "",
//    "emerg_contact_name": "",
//    "emerg_contact_no": "",
//    "role": "3",
//    "upload": "",
//    "status": "0",
//    "created_on": "2019-04-07 01:39:16",
//    "additional1": "",
//    "additional2": "",
//    "username": "alecxtudressafdsfs",
//    "password": "e10adc3949ba59abbe56e057f20f883e",
//    "contact_no": "",
//    "address": "",
//    "city": "",
//    "state": "",
//    "country": "",
//    "picture": "0",
//    "random_code": "55J9wPRi4j",
//    "last_pass_change": "2019-04-06 20:04:16"
//}

struct Employee:Codable{
    var status:String?
    var employee:[EmployeeData]?
}

struct EmployeeData :Codable{
    var name:String?
    var phone_number:String?
    var designation:String?
    var picture:String?
}
