//
//  ViewAllClaimsTableViewCell.swift
//  Digities
//
//  Created by vamsi on 08/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class ViewAllClaimsTableViewCell: UITableViewCell {

    @IBOutlet weak var claimDate: UILabel!
    @IBOutlet weak var claimName: UILabel!
    @IBOutlet weak var divisionName: UILabel!
    @IBOutlet weak var engagementCode: UILabel!
    @IBOutlet weak var expenseAmount: UILabel!
    @IBOutlet weak var reimbursementAmount: UILabel!
    
    @IBOutlet weak var clientDateLbl: UILabel!
    @IBOutlet weak var clientNameLbl: UILabel!
    @IBOutlet weak var divisionNameLbl: UILabel!
    @IBOutlet weak var engagementCodeLbl: UILabel!
    @IBOutlet weak var expenseAmountLbl: UILabel!
    @IBOutlet weak var reimbursementAmountLbl: UILabel!
    
    
    @IBOutlet weak var leftStackView: UIStackView!
    @IBOutlet weak var colonStackView: UIStackView!
    @IBOutlet weak var rightStackView: UIStackView!
    @IBOutlet weak var containerView: UIView!

    func configureCell(claimDetails:ViewAllClaimsDetails
        ,title:TitleType?
        ,currency:String,
         engagement:String){
        setupCellStyle(with: title)
        claimDate.text = (claimDetails.claim_date ?? "").getStringWithDateFormatter()
        claimName.text = claimDetails.client_name ?? ""
        divisionName.text = claimDetails.division_name ?? ""
        engagementCode.text = claimDetails.engagement_code ?? ""
        expenseAmount.text = currency + (claimDetails.total_expense_amt ?? "")
        reimbursementAmount.text = currency + (claimDetails.total_reimburse_amt ?? "")
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    
    func setupCellStyle(with title:TitleType?){
        setupUI()
        guard let title = title else{return}
        switch title {
        case .draft:
            containerView.backgroundColor = UIColor.gray.withAlphaComponent(0.2)
        case .pending:
            containerView.backgroundColor = UIColor.yellow.withAlphaComponent(0.2)
        case .rejected:
            containerView.backgroundColor = UIColor.red.withAlphaComponent(0.2)
        case .approved:
            containerView.backgroundColor = UIColor.green.withAlphaComponent(0.2)
        case .processed:
            containerView.backgroundColor = UIColor.blue.withAlphaComponent(0.2)
        case .viewAllClaims:
            containerView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        default:
            return
        }
        
        
    }
    
   
    func setupUI(){
      containerView.layer.cornerRadius = 22
      containerView.layer.shadowColor = UIColor.black.cgColor
      containerView.layer.shadowRadius = 0.1
      containerView.layer.shadowOpacity = 0.2
      containerView.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
    }
    
    
    
    
}
