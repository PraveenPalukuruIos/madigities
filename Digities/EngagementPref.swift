//
//  EngagementPref.swift
//  Digities
//
//  Created by vamsi on 11/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation

struct EngagementHead:Codable {
    var status:String?
    var preference:[EngagementData]?
}

struct EngagementData:Codable {
    var eng_preference:String?
}
