//
//  ExpenseDetailsTableViewCell.swift
//  Digities
//
//  Created by Santhana on 29/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class ExpenseDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var expenseDateLabel: UILabel!
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBOutlet weak var subCatgoryLabel: UILabel!
    
    @IBOutlet weak var label1: UILabel!
    
    @IBOutlet weak var labelContent1: UILabel!
    
    @IBOutlet weak var label2: UILabel!
    
    @IBOutlet weak var labelContent2: UILabel!
    
    @IBOutlet weak var label3: UILabel!
    
    @IBOutlet weak var labelContent3: UILabel!
    
    @IBOutlet weak var expenseAmountLabel: UILabel!
    
    @IBOutlet weak var expenseRemarkLabel: UILabel!
    
    @IBOutlet weak var reimbursableLabel: UILabel!
    
    @IBOutlet weak var reimbursableAmount: UILabel!
    
    @IBOutlet weak var changeableClientLabel: UILabel!
    
    @IBOutlet weak var pdfImageView1: UIImageView!
    
    @IBOutlet weak var pdfImageView2: UIImageView!
    
    @IBOutlet weak var containerStackView: UIStackView!
    
    @IBOutlet weak var containerView: UIView!
    
    
    var claimBills: [ClaimBillData] = []
    
    var isFrom:TitleType?
    var vc:ExpenseDetailsViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
    func configureCell(expDetails:ExpenseData,title:TitleType?){
        isFrom = title
        setupCellStyle(with: title)
        //        topImgView.image = #imageLiteral(resourceName: "Left_menu_viewall_claims")
        expenseDateLabel.text = (expDetails.expense_date ?? "").getStringWithDateFormatter()
        categoryLabel.text = expDetails.exp_cat_name ?? ""
        subCatgoryLabel.text = expDetails.exp_sub_cat_name
    
        switch expDetails.exp_cat_name {
        case Global.foodTypeExpense:
            label1.text = "Number of Employees*"
            label2.text = "Others"
            labelContent1.text = expDetails.no_of_employee
            labelContent2.text = expDetails.others
            let label3View = containerStackView.arrangedSubviews[5]
            label3View.isHidden = true
            
        case Global.stayTypeExpense:
            
            label1.text = "Number of Nights*"
            labelContent1.text = expDetails.no_of_nights
            let label2View = containerStackView.arrangedSubviews[4]
            label2View.isHidden = true
            let label3View = containerStackView.arrangedSubviews[5]
            label3View.isHidden = true
            
        case Global.travelTypeExpense:
            label1.text = "From*"
            label2.text = "To*"
            label3.text = "Distance"
            labelContent1.text = expDetails.from_loc
            labelContent2.text = expDetails.to_loc
            labelContent3.text = expDetails.distance
            
        default:
            print("Default")
        }
        
        if let currency = defaults.value(forKey: "currency") as? String{
            expenseAmountLabel.text = currency + (expDetails.exp_amount ?? "")
            reimbursableAmount.text = currency + (expDetails.reimbursable_amount ?? "")
        }
        
        expenseRemarkLabel.text = expDetails.expense_remarks ?? ""
        
        if expDetails.reimbursable == "1"{
            reimbursableLabel.text = "Yes"
            
        }else{
            reimbursableLabel.text = expDetails.reimbursable
            
        }
        
        if expDetails.chargable_to_client == "1"{
            changeableClientLabel.text = "Yes"
            
        }else{
            changeableClientLabel.text = expDetails.chargable_to_client
            
        }
        
        
        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(zoomBottomImage))
        pdfImageView2.addGestureRecognizer(tap)
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(zoomTopImage))
        pdfImageView1.addGestureRecognizer(tapGest)
    }
    
    @objc func zoomBottomImage(){
        
        if(claimBills.count == 2){
            if let vc = vc{
                let path: String = claimBills[1].filename ?? ""
                vc.showPDF(path: BaseUrl.getPDFURL + path)
            }
        }
        
    }
    
    @objc func zoomTopImage(){
       
        if(claimBills.count == 1){
            if let vc = vc{
                //  pdfImageView2.zoomAnimation(inViewController: vc)
               let path: String = claimBills[0].filename ?? ""
                vc.showPDF(path: BaseUrl.getPDFURL + path)
                
            }
        }
    }
    
    
    func setupClaimBill(claimDetails: [ClaimBillData]){
        //        let url =  "http://projectdemo.co.in/expensetracking/Androidapi/uploads/Fast_food_meal.jpg"
        //       let url = " http://projectdemo.co.in/expensetracking/assets/uploads/expense/bills/Fast_food_meal.jpg"
        
        
       // if  let imageName = claimDetails.original{
       //     if let url = URL(string: BaseUrl.getImagePath + imageName){
       //         pdfImageView2.sd_setImage(with: url, completed: nil)
       //     }
      //  }
        

        claimBills = claimDetails
        if(claimDetails.count == 2){
            pdfImageView1.image = UIImage(named: "iconpdf")
             pdfImageView2.image = UIImage(named: "iconpdf")
        }else if(claimDetails.count == 1){
            pdfImageView1.image = UIImage(named: "iconpdf")
        }
        
    }
    
    func setupCellStyle(with title:TitleType?){
        setupUI()
        guard let title = title else{return}
        switch title {
        case .draft:
            containerView.backgroundColor = UIColor.gray.withAlphaComponent(0.2)
        case .pending:
            containerView.backgroundColor = UIColor.yellow.withAlphaComponent(0.2)
        case .rejected:
            containerView.backgroundColor = UIColor.red.withAlphaComponent(0.2)
        case .approved:
            containerView.backgroundColor = UIColor.green.withAlphaComponent(0.2)
        case .processed:
            containerView.backgroundColor = UIColor.blue.withAlphaComponent(0.2)
        case .viewAllClaims:
            containerView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        default:
            return
        }
        
        
    }
    
    
    func setupUI(){
        containerView.layer.cornerRadius = 25
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowRadius = 0.1
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
    }
    
    

}
