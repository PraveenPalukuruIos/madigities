//
//  detailClaimTableViewCell.swift
//  Digities
//
//  Created by vamsi on 08/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import UIKit

class DetailClaimTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var leftStackView: UIStackView!
    @IBOutlet weak var colonStackView: UIStackView!
    @IBOutlet weak var rightStackView: UIStackView!
    
    
    @IBOutlet weak var expenseDatelbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var totalExpenseLbl: UILabel!
    
    
    
    @IBOutlet weak var expenseDate: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var totalExpense: UILabel!
    
    
    
    func configureCell(claimDetails:DetailClaimsData,title:TitleType?){
        setupUI()
        setupCellStyle(with: title)
        expenseDate.text = claimDetails.expense_date ?? ""
        category.text = claimDetails.exp_cat_name ?? ""
        totalExpense.text = claimDetails.exp_amount ?? ""
    }
    
    func setupCellStyle(with title:TitleType?){
        guard let title = title else{return}
        switch title {
        case .draft:
            containerView.backgroundColor = UIColor.gray.withAlphaComponent(0.2)
        case .pending:
            containerView.backgroundColor = UIColor.yellow.withAlphaComponent(0.2)
        case .rejected:
            containerView.backgroundColor = UIColor.red.withAlphaComponent(0.2)
        case .approved:
            containerView.backgroundColor = UIColor.green.withAlphaComponent(0.2)
        case .processed:
            containerView.backgroundColor = UIColor.blue.withAlphaComponent(0.2)
        case .viewAllClaims:
            containerView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        default:
            return
        }
        
        
    }
    
    
    func setupUI(){
        containerView.layer.cornerRadius = 20
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowRadius = 0.1
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
    }
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
}
