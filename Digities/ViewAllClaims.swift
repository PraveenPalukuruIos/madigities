//
//  ViewAllClaims.swift
//  Digities
//
//  Created by vamsi on 08/06/19.
//  Copyright © 2019 vamshi krishna. All rights reserved.
//

import Foundation
//{
//    "status": "success",
//    "data": [{
//    "id": "1594",
//    "comp_claim_id": null,
//    "comp_id": "1",
//    "client_id": "2",
//    "client_name": "Client2",
//    "division_id": "70",
//    "division_name": "computerss",
//    "employee_id": "1",
//    "engagement_code": "",
//    "claim_date": "2019-06-07",
//    "additional1": "4",
//    "additional2": "",
//    "status": "1",
//    "total_expense_amt": "18",
//    "total_reimburse_amt": "18",
//    "total_approved_amt": "0",
//    "total_rejected_amt": "0",
//    "total_paid_amt": "0",
//    "claim_status": "1",
//    "approved_by": "0",
//    "created_on": "2019-06-07 12:56:15",
//    "created_by": " aravind"
//}


struct ViewAllClaims:Codable {
    var status:String?
    var data:[ViewAllClaimsDetails]?
}

struct ViewAllClaimsDetails:Codable {
    var comp_id:String?
    var client_id:String?
    var id:String?
    var division_id:String?
    var division_name:String?
    var employee_id:String?
    var claim_date:String?
    var client_name:String?
    var engagement_code:String?
    var total_expense_amt:String?
    var total_reimburse_amt:String?
    var total_rejected_amt:String?
    var total_approved_amt:String?
    var total_paid_amt:String?
    var created_by:String?
    var created_on:String?
}
